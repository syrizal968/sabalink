<x-layouts.app-layout title="About">
    <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">About Sabalink Management</h5>
        <span>This project developer by <a href="https://www.youtube.com/@BarengSaya" target=”_blank”>BarengSaya</a></span>
        <span>Don't forget to subscribe my channel</span>
    </div>
    <div class="card-body col-lg-8">
        <p class="card-text">
            {{ __('Aplikasi ini merupakan aplikasi yang di dedikasikan untuk para pejuang RT RW Net.') }}
            {{ __('Dikembangkan dari aplikasi Griya-Net berbasis framework Laravel.') }}
            {{ __('Silahkan subscribe chanel kami jika bermanfaat untuk kalian.') }}
        </p>
        <div class="col-md-12 mb-3">
            <label for="periode"><strong>{{ __('1. Install Auto Isolir') }}</strong></label>
            <div class="input-group">
                <iframe src="{{ url('https://www.youtube.com/embed/dB7s_lsq2yM') }}" width="560" height="315" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
        <div class="col-md-12 mb-3">
            <label for="periode"><strong>{{ __('2. How to use Customer Management') }}</strong></label>
            <div class="input-group">
                <iframe src="{{ url('https://www.youtube.com/embed/E0dBOwqvwZ4') }}" width="560" height="315" frameborder="0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
	 @section('custom_scripts')
    <script data-name="NBJ-Widget" data-cfasync="false" src="https://www.nihbuatjajan.com/javascripts/widget.prod.min.js?u=griyanet" data-id="griyanet" data-domain="https://www.nihbuatjajan.com" data-description="" data-message="" data-color="#FF813F" data-position="Right" data-x_margin="18" data-y_margin="18">
    </script>

    @endsection
</x-layouts.app-layout>
