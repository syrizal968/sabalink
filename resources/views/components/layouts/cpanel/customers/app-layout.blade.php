<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Ari Purnawan">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{$title ?? ''}} | {{ __('Customer Management') }}</title>
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('images/favicon/apple-touch-icon.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('images/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/favicon/favicon-16x16.png')}}">
    <link rel="icon" type="image/x-icon" href="{{ asset('images/favicon/favicon.png') }}" />
    <link rel="manifest" href="{{asset('assets/site.webmanifest')}}">
    <link rel="mask-icon" href="{{asset('images/favicon/safari-pinned-tab.svg')}}" color="#5bbad5">
    @vite('resources/sass/app.scss')
    <link href="{{ asset('css/fontawsome-free-all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/notify/notify.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/sweet-alert/sweet-alert.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/datatables/datatables.min.css')}}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
    @yield('custom_styles')

</head>

<body id="page-top">
    <div id="wrapper">
        <x-layouts.cpanel.customers.navigation />
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <x-layouts.top-bar />
                @if ($errors->any())
                <div class="container-fluid">
                    <div class="card shadow mb-4">
                        <div class="card-body">
                            <div class="form-group mt-4 mb-4">
                                <div class="pull-right alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                @endif
                <div class="container-fluid">
                    <div class="card shadow mb-4">
                        {{ $slot }}
                    </div>
                </div>
            </div>
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Sabalink Management V{{ App\Models\Websystem::where('title', 'Customer Management')->value('version') }}</span>
                        <div>
                            <span>
                                Copyright &copy; SABALiNK {{ date('Y')}} - <a href="https://www.barengsaya.com" target="_blank"><i class="fab fa-youtube" style="color:red"></i> @griya-net</a>
                            </span>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a href="{{ route('logout') }}" class="btn btn-primary" onclick="event.preventDefault(); this.closest('form').submit();">
                            <i class="mr-2 fas fa-sign-out-alt"></i>
                            {{ __('Log Out') }}
                        </a>
                    </form>

                </div>
            </div>
        </div>
    </div>
    @vite('resources/js/app.js')
    <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/jquery.easing-1.4.1.min.js') }}"></script>
    <script src="{{asset('js/bootstrap.bundle.min.js')}}" crossorigin="anonymous"></script>
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script src="{{asset('assets/notify/notify.min.js')}}"></script>
    <script src="{{asset('assets/sweet-alert/sweet-alert.min.js')}}"></script>
    <script src="{{asset('assets/vendor/datatables/datatables.min.js')}}"></script>
    @yield('custom_scripts')
    @if ($message = Session::get('success'))
    <script>
        Swal.fire({
            icon: 'success',
            text: '{{ $message }}',
        })
    </script>
    @endif
    @if ($message = Session::get('error'))
    <script>
        Swal.fire({
            icon: 'error',
            text: '{{ $message }}',
        })
    </script>
    @endif
</body>
</html>
