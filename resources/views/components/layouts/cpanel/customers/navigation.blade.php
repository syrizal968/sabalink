<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-rss"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SABALiNK BILLING</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item @if(request()->routeIs('home')) active @endif">
        <a class="nav-link" href="{{ route('home') }}">
            <i class="fas fa-home"></i>
            <span>{{ __('Home') }}</span>
        </a>
    </li>


    <!-- Nav Item - Tables -->

    @canany(['customer-list', 'customer-edit', 'customer-delete', 'customer-create', 'secret-edit'])
    <li class="nav-item @if(request()->routeIs('customers*')) active @endif">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCustomer" aria-expanded="true" aria-controls="collapseCustomer" style="padding-top: inherit;">
            <i class="fas fa-fw fa-users"></i>
            <span>{{ __('navigation.customers.nav-item') }}</span>
        </a>
        <div id="collapseCustomer" class="collapse collapse @if(request()->routeIs('customers*')) show @endif" aria-labelledby="headingBilling" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                @can('customer-create')
                <a class="collapse-item @if(request()->routeIs('customers.create')) active @endif" href="{{ route('customers.create') }}">{{ __('navigation.customers.create') }}</a>
                @endcan
                @canany(['customer-list', 'secret-edit', 'customer-delete', 'customer-edit'])
                <a class="collapse-item @if(request()->routeIs('customers.index')) active @endif" href="{{ route('customers.index') }}">{{ __('navigation.customers.index') }}</a>
                @endcanany
                @canany(['customer-list', 'secret-edit', 'customer-delete', 'customer-edit'])
                <a class="collapse-item @if(request()->routeIs('customers.show.offline')) active @endif" href="{{ route('customers.show.offline') }}">{{ __('navigation.customers.show-offline') }}</a>
                @endcanany
            </div>

        </div>

    </li>
    @endcanany



    <!-- Divider -->
    <hr class="sidebar-divider">
    @if(request()->routeIs('customers.index'))
    <li class="nav-item active">

        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSearchCustomer" aria-expanded="true" aria-controls="collapseSearchCustomer" style="padding-top: inherit;" style="padding-top: inherit;">
            <i class="fas fa-search"></i>
            <span>{{ __('billing.header.search') }}</span>
        </a>
        <div class="collapse" id="collapseSearchCustomer">
            <div class="card-header py-3 rounded">
                <form>
                    <div class="row align-items-center mb-1">
                        <input type="search" class="form-control" placeholder="{{ __('customer.placeholder.name') }}" name="search_name">
                    </div>
                    <div class="row align-items-center mb-1">
                        <select name="search_address" class="form-control">
                            <option value="">{{ __('customer.placeholder.select-address') }}</option>
                            <option value="NULL">{{ __('customer.placeholder.without-address') }}</option>
                            @foreach ($address as $addrr)
                            <option value="{{ $addrr }}">{{ $addrr }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row align-items-center mb-1">
                        <select name="disabled" class="form-control">
                            <option value="">All Status</option>
                            <option value="false">Enable</option>
                            <option value="true">Disable</option>
                        </select>
                    </div>
                    <div class="row align-items-center mb-1">
                        {!! Form::select('server_search', $allServers, null, array('placeholder' => 'All Server','class' => 'form-control', 'id' => 'server-dropdown')) !!}
                    </div>
                    <div class="row align-items-center mb-1">
                        <select name="search_paket" id="paket-dropdown" class="form-control">
                            <option value="">All Paket</option>
                        </select>
                    </div>

                    <div class="row align-items-center mt-3">
                        <span>
                            <button class="btn btn-success btn-sm" type="submit"><i class="fas fa-search"></i> {{ __('customer.button.search') }}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </li>
    @endif


    <div class="text-center d-none d-md-inline pt-4">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>



</ul>
<!-- End of Sidebar -->
