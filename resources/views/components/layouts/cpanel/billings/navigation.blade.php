<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-rss"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SABALiNK Management</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item @if(request()->routeIs('home')) active @endif">
        <a class="nav-link" href="{{ route('home') }}">
            <i class="fas fa-home"></i>
            <span>{{ __('Home') }}</span>
        </a>
    </li>


    <!-- Nav Item - Tables -->

    @canany(['billing-list', 'billing-create', 'billing-edit', 'billing-delete'])
    <li class="nav-item @if(request()->routeIs('billings*')) active @endif">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBilling" aria-expanded="true" aria-controls="collapseBilling" style="padding-top: inherit;">
            <i class="fas fa-money-check-alt"></i>
            <span>{{ __('navigation.billings.nav-item') }}</span>
        </a>
        <div id="collapseBilling" class="collapse collapse @if(request()->routeIs('billings*')) show @endif" aria-labelledby="headingBilling" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                @can('billing-create')
                <a class="collapse-item @if(request()->routeIs('billings.create')) active @endif" href="{{ route('billings.create') }}">{{ __('navigation.billings.create') }}</a>
                <a class="collapse-item @if(request()->routeIs('billings.create.one')) active @endif" href="{{ route('billings.create.one') }}">{{ __('navigation.billings.add') }}</a>

                @endcan
            </div>
            <div class="bg-white py-2 collapse-inner rounded">
                @canany(['billing-list', 'billing-edit', 'billing-delete'])
                <a class="collapse-item @if(request()->routeIs('billings.index')) active @endif" href="{{ route('billings.index') }}">{{ __('navigation.billings.show') }}</a>

                @endcanany
            </div>
        </div>
    </li>
    @endcanany



    <!-- Divider -->
    <hr class="sidebar-divider">
    @if(request()->routeIs('billings.index'))
    <li class="nav-item active">

        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSearchCustomer" aria-expanded="true" aria-controls="collapseSearchCustomer" style="padding-top: inherit;" style="padding-top: inherit;">
            <i class="fas fa-search"></i>
            <span>{{ __('billing.header.search') }}</span>
        </a>
        <div class="collapse" id="collapseSearchCustomer">
            <div class="card-header py-3 rounded">
                <form>
                    <div class="row align-items-center mb-1">
                        {!! Form::text('search_name', null, array('placeholder' => __('billing.placeholder.customer-name'),'class' => 'form-control')) !!}
                    </div>
                    <div class="row align-items-center mb-1">
                        <select name="search_address" class="form-control">
                            <option value="">{{ __('customer.placeholder.select-address') }}</option>
                            <option value="NULL">{{ __('customer.placeholder.without-address') }}</option>
                            @foreach ($address as $addrr)
                            <option value="{{ $addrr }}">{{ $addrr }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row align-items-center mb-1">
                        <select name="search_month" class="form-control">
                            <option value="">{{ __('billing.placeholder.select-month') }}</option>
                            @foreach(\Carbon\CarbonPeriod::create(now(), '1 month', now()->addMonths(11)) as $date)
                            <option value="{{ $date->format('m') }}">
                                {{ $date->format('F') }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row align-items-center mb-1">
                        {!! Form::select('search_year', $years, null, array('placeholder' => __('billing.placeholder.select-year'),'class' => 'form-control')) !!}
                    </div>
                    <div class="row align-items-center mb-1">
                        <select name="search_status" class="form-control">
                            <option value="">{{ __('billing.placeholder.select.status') }}</option>
                            <option value="BL">{{ __('billing.placeholder.select.unpaid') }}</option>
                            <option value="PL">{{ __('billing.placeholder.select.paylater') }}</option>
                            <option value="LS">{{ __('billing.placeholder.select.paid') }}</option>
                        </select>
                    </div>
                    <div class="row align-items-center mb-1">
                        <div class="form-switch">
                            <input class="form-check-input" value="true" type="checkbox" name="cetak_pdf">
                            <label class="form-check-label">
                                {{ __('Cetak PDF?') }}
                            </label>
                        </div>
                    </div>
                    <div class="row align-items-center mt-3">
                        <span>
                            <button class="btn btn-success btn-sm" type="submit"><i class="fas fa-search"></i> {{ __('customer.button.search') }}</button>
                        </span>
                    </div>
                </form>
            </div>
        </div>
    </li>
    @endif


    <div class="text-center d-none d-md-inline pt-4">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>



</ul>
<!-- End of Sidebar -->
