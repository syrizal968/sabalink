<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-rss"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SABALiNK Billing</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item @if (request()->routeIs('home')) active @endif">
        <a class="nav-link" href="{{ route('home') }}">
            <i class="fas fa-home"></i>
            <span>{{ __('Home') }}</span>
        </a>
    </li>


    <!-- Nav Item - Tables -->

    @canany(['customer-list', 'customer-edit', 'customer-delete', 'customer-create', 'secret-edit'])
        <li class="nav-item @if (request()->routeIs('customers*')) active @endif">
            <a class="nav-link collapsed" href="{{ route('customers.index') }}" style="padding-top: inherit;">
                <i class="fas fa-fw fa-users"></i>
                <span>{{ __('navigation.customers.nav-item') }}</span>
            </a>

        </li>
    @endcanany
    @canany(['billing-list', 'billing-create', 'billing-edit', 'billing-delete'])
        <li class="nav-item @if (request()->routeIs('billings*')) active @endif">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBilling"
                aria-expanded="true" aria-controls="collapseBilling" style="padding-top: inherit;">
                <i class="fas fa-money-check-alt"></i>
                <span>{{ __('navigation.billings.nav-item') }}</span>
            </a>
            <div id="collapseBilling" class="collapse collapse @if (request()->routeIs('billings*')) show @endif"
                aria-labelledby="headingBilling" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    @can('billing-create')
                        <a class="collapse-item @if (request()->routeIs('billings.create')) active @endif"
                            href="{{ route('billings.create') }}">{{ __('navigation.billings.create') }}</a>
                        <a class="collapse-item @if (request()->routeIs('billings.create.one')) active @endif"
                            href="{{ route('billings.create.one') }}">{{ __('navigation.billings.add') }}</a>
                    @endcan
                </div>
                <div class="bg-white py-2 collapse-inner rounded">
                    @canany(['billing-list', 'billing-edit', 'billing-delete'])
                        <a class="collapse-item @if (request()->routeIs('billings.index')) active @endif"
                            href="{{ route('billings.index') }}">{{ __('navigation.billings.show') }}</a>
                    @endcanany
                </div>
            </div>
        </li>
    @endcanany
    @canany(['packet-list', 'packet-create', 'packet-edit', 'packet-delete'])
        <li class="nav-item @if (request()->routeIs('packets*')) active @endif">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePacket"
                aria-expanded="true" aria-controls="collapsePacket" style="padding-top: inherit;">
                <i class="fas fa-box"></i>
                <span>{{ __('navigation.pakets.nav-item') }}</span>
            </a>
            <div id="collapsePacket" class="collapse collapse @if (request()->routeIs('packets*')) show @endif"
                aria-labelledby="headingBilling" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    @can('packet-create')
                        <a class="collapse-item @if (request()->routeIs('packets.create')) active @endif"
                            href="{{ route('packets.create') }}">{{ __('navigation.pakets.create') }}</a>
                    @endcan
                    @canany(['packet-list', 'packet-edit', 'packet-delete'])
                        <a class="collapse-item @if (request()->routeIs('packets.index')) active @endif"
                            href="{{ route('packets.index') }}">{{ __('navigation.pakets.index') }}</a>
                    @endcanany
                </div>
            </div>
        </li>
    @endcanany



    <!-- Divider -->
    <hr class="sidebar-divider">
    @canany(['user-list', 'user-create', 'user-edit', 'user-delete', 'role-list', 'role-edit', 'role-delete',
        'server-list', 'server-edit', 'server-delete', 'server-create'])
        <li class="nav-item @if (Request::is('admin/settings*')) active @endif">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseSetting"
                aria-expanded="true" aria-controls="collapseSetting" style="padding-top: inherit;">
                <i class="fas fa-user-shield"></i>
                <span>{{ __('Setting') }}</span>
            </a>
            <div id="collapseSetting" class="collapse collapse @if (Request::is('admin/settings*')) show @endif"
                aria-labelledby="headingBilling" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    @canany(['user-list', 'user-create', 'user-edit', 'user-delete'])
                        <a class="collapse-item @if (request()->routeIs('users*')) active @endif"
                            href="{{ route('users.index') }}">
                            <i class="fas fa-user-cog"></i>
                            <span> {{ __('dashboard.users') }}</span>
                        </a>
                    @endcanany

                    @canany(['role-list', 'role-edit', 'role-delete'])
                        <a class="collapse-item @if (request()->routeIs('roles*')) active @endif"
                            href="{{ route('roles.index') }}">
                            <i class="fas fa-user-shield"></i>
                            <span> Roles</span></a>
                    @endcanany
                    @canany(['server-list', 'server-delete', 'server-edit'])
                        <a class="collapse-item @if (request()->routeIs('paymentgateway*')) active @endif"
                            href="{{ route('paymentgateway.index') }}">
                            <i class="fas fa-server"></i>
                            <span>Payment Gateway</span>
                        </a>
                        <a class="collapse-item @if (request()->routeIs('servers*')) active @endif"
                            href="{{ route('servers.index') }}">
                            <i class="fas fa-server"></i>
                            <span>Servers</span>
                        </a>

                        <a class="collapse-item @if (request()->routeIs('autoisolir*')) active @endif"
                            href="{{ route('autoisolir.index') }}">
                            <i class="fas fa-user-times"></i>
                            <span>Auto Isolir</span>
                        </a>

                        <a class="collapse-item  @if (request()->routeIs('routers*')) active @endif"
                            href="{{ route('routers.index') }}">
                            <i class="fas fa-wifi"></i>
                            <span>Routers</span>
                        </a>

                        <a class="collapse-item @if (request()->routeIs('customer.setting')) active @endif"
                            href="{{ route('customer.setting') }}">
                            <i class="fas fa-clone"></i>
                            <span>Customers</span>
                        </a>

                        <a class="collapse-item @if (request()->routeIs('paket.setting')) active @endif"
                            href="{{ route('paket.setting') }}">
                            <i class="fas fa-clone"></i>
                            <span>Packages</span>
                        </a>

                        <a class="collapse-item @if (request()->routeIs('setting.system.index')) active @endif"
                            href="{{ route('setting.system.index') }}">
                            <i class="fas fa-cog"></i>
                            <span>System</span>
                        </a>

                        <a class="collapse-item @if (request()->routeIs('settings.update*')) active @endif"
                            href="{{ route('settings.update.index') }}">
                            <i class="fas fa-upload"></i>
                            <span>Update</span>
                        </a>
                    @endcanany
                </div>

            </div>
        </li>
    @endcanany
    <hr class="sidebar-divider">
    {{-- <li class="nav-item @if (request()->routeIs('about')) active @endif">
        <a class="nav-link" href="{{ route('about') }}">
            <i class="fas fa-fw fa-eye"></i>
            <span>{{ __('About') }}</span></a>
    </li> --}}
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline pt-4">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
