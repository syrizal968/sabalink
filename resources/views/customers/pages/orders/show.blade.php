<x-layouts.customers.app-layout title="Order #{{ $order->number }}">

    <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">{{ __('Data Order #') }}{{ $order->number }}</h5>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-condensed">
            <tr>
                <td>ID</td>
                <td><b>#{{ $order->number }}</b></td>
            </tr>
            <tr>
                <td>Nama</td>
                <td><b>{{ $order->billing->customer_name }}</b></td>
            </tr>
            <tr>
                <td>Periode</td>
                <td><b>{{ $order->billing->billing_periode->name }}</b></td>
            </tr>
            <tr>
                <td>Paket</td>
                <td><b>{{ $order->billing->paket_name }}</b></td>
            </tr>
            <tr>
                <td>Total Harga</td>
                <td><b>@moneyIDR($order->billing->paket_price)</b></td>
            </tr>
            <tr>
                <td>Status Pembayaran</td>
                <td><b>
                        @if ($order->payment_status == '1')
                            {{ __('Menunggu Pembayaran') }}
                        @elseif ($order->payment_status == 2)
                            {{ __('Lunas') }}
                        @elseif ($order->payment_status == 3)
                            {{ __('billing.status.expired') }}
                        @else
                            {{ __('billing.status.cancelled') }}
                        @endif
                    </b></td>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td><b>{{ $order->created_at->format('d M Y H:i') }}</b></td>
            </tr>
        </table>
    </div>
    <div class="card-footer">
        @if ($order->payment_status == 1)
            <button class="btn btn-primary" id="pay-button"><i class="far fa-money-bill-alt"></i> Bayar Sekarang</button>
        @elseif ($order->payment_status == 2)

        @elseif ($order->payment_status == 3)
        @endif
    </div>


    @section('custom_scripts')
        <script src="https://app.sandbox.midtrans.com/snap/snap.js" data-client-key="{{ $clientKey }}"></script>

        <script>
            const payButton = document.querySelector('#pay-button');

            payButton.addEventListener('click', function(e) {
                e.preventDefault();

                snap.pay('{{ $snapToken }}', {
                    onSuccess: function(result) {
                        console.log('Payment success:', result);
                        sendResponseToForm(result);
                    },
                    onPending: function(result) {
                        console.log('Payment pending:', result);
                        sendResponseToForm(result);
                    },
                    onError: function(result) {
                        console.log('Payment error:', result);
                    },
                    onClose: function() {
                        console.log('Customer closed the popup without finishing the payment');
                        Swal.fire({
                            icon: 'error',
                            text: 'Customer closed the popup without finishing the payment',
                        });
                    }
                });
            });

            function sendResponseToForm(result) {
                fetch("{{ route('payment.success') }}", {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                        body: JSON.stringify({
                            order_id: '{{ $order->number }}',
                            transaction_status: result.transaction_status,
                            transaction_id: result.transaction_id,
                            payment_type: result.payment_type,
                            payment_time: result.payment_time,
                            pdf_url: result.pdf_url,
                            gross_amount: result.gross_amount
                        })
                    })
                    .then(response => response.json())
                    .then(data => {
                        console.log('Payment response:', data);
                        if (data.status === 'success') {
                            activateCustomer();
                        } else {
                            Swal.fire({
                                icon: 'error',
                                text: 'Payment failed, please try again.',
                            });
                        }
                    })
            }

            function activateCustomer() {
                fetch("{{ route('customer.activations', $order->billing->customer) }}", {
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        }
                    })
                    .then(response => {
                        console.log('Activation response:', response);
                        if (response.redirected) {
                            window.location.href = response.url;
                        } else {
                            return response.json();
                        }
                    })
                    .then(data => {
                        console.log('Activation data:', data);
                        if (data && data.status === 'success') {
                            Swal.fire({
                                icon: 'success',
                                text: 'Payment successful!',
                            }).then(() => {
                                window.location.href = "{{ route('customer.order.show', $order->id) }}";
                            });
                        }
                    })
            }
        </script>


        @if ($message = Session::get('success'))
            <script>
                Swal.fire({
                    icon: 'success',
                    text: '{{ $message }}',
                })
            </script>
        @endif
    @endsection
</x-layouts.customers.app-layout>
