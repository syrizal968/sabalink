<x-layouts.customers.app-layout title="Customers">
    <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">
            {{ __('customer.page.header.index') }} {{ auth()->guard('customer')->user()->name }}
        </h5>
    </div>
    <div class="card-body col-lg-12">
        <div class="table-responsive">
            <table id="serverTable" cellspacing="0" width="100%" class="table no-footer">
                <tr class="table-borderless">
                    <td width="150px"><strong>{{ __('customer.label.paket') }}</strong></td>
                    <td width="380px">{{ __(':') }} {{ $customer->paket->name ?? 'No Data' }}</td>
                    <td width="150px"><strong>{{ __('customer.label.bill') }}</strong></td>
                    <td width="380px">{{ __(':') }} <strong>@moneyIDR($customer->paket->price ?? 0)</strong> / bulan</td>
                </tr>
                <tr class="table-borderless">
                    <td><strong>{{ __('customer.page.label.due-date') }}</strong></td>
                    <td>{{ __(':') }} {{ $customer->activation_date ?? 'No Data' }}</td>
                </tr>
            </table>
            <p><strong>{{ __('customer.page.label.bill-not-payed') }}</strong> {{ $billings->count() }}
                {{ __('tagihan') }}</p>
            <table id="customersTable" cellspacing="0" width="100%"
                class="table datatable-loading no-footer sortable searchable">
                <thead>
                    <tr>
                        <th>{{ __('customer.table.no') }}</th>
                        <th>{{ __('billing.table.periode') }}</th>
                        <th>{{ __('billing.table.paket-name') }}</th>
                        <th>{{ __('billing.table.paket-price') }}</th>
                        <th width="180px">{{ __('billing.table.action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($billings as $index => $billing)
                        <tr>
                            <td>{{ $index + 1 }}</td>
                            <td>{{ $billing->billing_periode->name }}</td>
                            <td>{{ $billing->paket_name }}</td>
                            <td>@moneyIDR($billing->paket_price)</td>
                            <td class="d-flex items-center justify-content">
                                <a href="{{ route('customer.payment.show', $billing->id) }}" class="btn btn-success">
                                    Lihat
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="3"><strong>Total</strong></td>
                        <td><strong>@moneyIDR($billings->sum('paket_price'))</strong></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>

</x-layouts.customers.app-layout>
