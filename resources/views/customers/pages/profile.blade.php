<x-layouts.customers.app-layout title="Customers">

    <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">{{ __('customer.page.header.profile') }}</h5>

    </div>

    <div class="card-body col-lg-12">
        <div class="table-responsive">
            <table id="serverTable" cellspacing="0" width="100%" class="table no-footer">
                <tr class="table-borderless">
                    <td width="150px"><strong>{{ __('customer.label.name') }}</strong></td>
                    <td width="380px">{{ __(':') }} {{ $customer->name }}</td>
                    <td width="150px"><strong>{{ __('customer.label.email') }}</strong></td>
                    <td width="380px">{{ __(':') }} {{ $customer->email }}</td>
                </tr>

                <tr class="table-borderless">
                    <td><strong>{{ __('customer.label.address') }}</strong></td>
                    <td>{{ __(':') }} {{ $customer->address }}</td>
                    <td><strong>{{ __('customer.label.contact') }}</strong></td>
                    <td>{{ __(':') }} {{ $customer->contact }}</td>
                </tr>


            </table>
        </div>
    </div>


</x-layouts.customers.app-layout>
