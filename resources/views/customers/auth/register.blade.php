@extends('layouts.guest')

@section('content')
    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-xl-10 col-lg-12 col-md-9">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <span>
                        @if (isset($message))
                            <div class="alert alert-danger">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ $message }}
                            </div>
                        @endif
                    </span>
                    <div class="row justify-content-center">
                        {{-- <div class="col-lg-6 d-none d-lg-block bg-login-image-secondary"></div> --}}
                        <div class="col-lg-10">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">Create an Account!</h1>
                                </div>
                                <form class="user" action="{{ route('customer.add') }}" method="POST">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="col-md-12 mb-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="exampleFirstName"
                                                        placeholder="Nama Lengkap" name="name">
                                                </div>
                                                <div class="form-group">
                                                    <input type="password" class="form-control" id="exampleInputPassword"
                                                        placeholder="Password" name="password">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="col-md-12 mb-3">
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="exampleInputEmail"
                                                        placeholder="Email Address" name="email">
                                                </div>
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <select name="paket_id" id="paket-dropdown" class="form-control">
                                                            <option value="">Pilih Paket</option>
                                                            @foreach ($paket as $item)
                                                                <option value="{{ $item->id }}">{{ $item->name }} -
                                                                    @moneyIDR($item->price)
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="col-md-12 mb-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="exampleFirstName"
                                                        placeholder="Kontak" name="contact">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="col-md-12 mb-3">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="exampleFirstName"
                                                        placeholder="Alamat  : contoh Desa pelengan Daya, Kec Palenggaan, Kab Pamekasan "
                                                        name="address">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                        Register Account
                                    </button>
                                    <a href="/customer/login" class="btn btn-success btn-user btn-block">
                                        Login
                                    </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection
