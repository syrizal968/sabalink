<x-layouts.cpanel.customers.app-layout title="Create Customer">

    <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">{{ __('customer.header.create') }}</h5>
    </div>

    <div class="row">
        <div class="col-lg-12">
            {!! Form::open(array('route' => 'customers.store','method'=>'POST')) !!}
            <div class="card-body row">
                <div class="col-lg-6">
                    <div class="row align-items-center">
                        <div class="col-md-6 mb-3">
                            <label for="name"><strong>{{ __('customer.label.name') }}</strong></label>
                            <div class="input-group ">
                                {!! Form::text('name', null, array('placeholder' => __('customer.placeholder.name'),'class' => 'form-control')) !!}
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-user"></span>
                                    </div>
                                </div>
                            </div>
                            @if ($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="email"><strong>{{ __('customer.label.email') }}</strong></label>
                            <div class="input-group">
                                {!! Form::text('email', null, array('placeholder' => __('customer.placeholder.email'),'class' => 'form-control')) !!}
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-envelope"></span>
                                    </div>
                                </div>
                            </div>
                            @if ($errors->has('email'))
                            <span class="text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6 mb-3">
                            <label for="server"><strong>{{ __('customer.label.server') }}</strong></label>
                            <div class="input-group">
                                {!! Form::select('mikrotik_id', $mikrotiks, null, array('placeholder' => 'Select Server','class' => 'form-control', 'id' => 'server-dropdown')) !!}
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-server"></span>
                                    </div>
                                </div>
                                @if ($errors->has('mikrotik_id'))
                                <span class="text-danger">{{ $errors->first('mikrotik_id') }}</span>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="paket"><strong>{{ __('customer.label.paket') }}</strong></label>
                            <div class="input-group">
                                <select name="paket_id" id="paket-dropdown" class="form-control">
                                    <option value="">Pilih Paket</option>
                                </select>
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-box"></span>
                                    </div>
                                </div>
                            </div>

                            @if ($errors->has('paket_id'))
                            <span class="text-danger">{{ $errors->first('paket_id') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="row align-items-center">
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-12">
                            <label for="discount"><strong>{{ __('customer.label.discount') }}</strong></label>
                        </div>
                        <div class="col-md-12 mb-3 row">
                            <div class="col-md-2 mx-0">
                                {!! Form::text('discount', null, array('placeholder' => __('customer.placeholder.discount'),'class' => 'form-control')) !!}
                            </div>
                            <div class=" py-2">
                                <strong>%</strong>
                            </div>
                            <div class="col-md-12">
                                @if ($errors->has('discount'))
                                <span class="text-danger">{{ $errors->first('discount') }}</span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-md-12 mb-3">
                            <label for="service">{{ __('pppoe.secret.service') }}</label>
                            <div class="input-group">
                                {!! Form::select('ppp_type_id', $pppservices, null, array('placeholder' => 'Select Service','class' => 'form-control', 'id' => 'ppp_types_id')) !!}
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-cog"></span>
                                    </div>
                                </div>
                            </div>
                            @if ($errors->has('ppp_type_id'))
                            <span class="text-danger">{{ $errors->first('ppp_type_id') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-md-6 mb-3">
                            <label for="password"><strong>{{ __('customer.label.password') }}</strong></label>
                            <div class="input-group">
                                {!! Form::password('password', array('placeholder' => __('customer.placeholder.password'),'class' => 'form-control')) !!}
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-lock"></span>
                                    </div>
                                </div>
                            </div>
                            @if ($errors->has('password'))
                            <span class="text-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="password_confirmation"><strong>{{ __('customer.label.confirm-password') }}</strong></label>
                            <div class="input-group">
                                {!! Form::password('password_confirmation', array('placeholder' => __('customer.placeholder.confirm-password'),'class' => 'form-control')) !!}
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-lock"></span>
                                    </div>
                                </div>
                            </div>
                            @if ($errors->has('password_confirmation'))
                            <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-12 mb-3">
                            <label for="address"><strong>{{ __('customer.label.address') }}</strong></label>
                            <div class="input-group">
                                {!! Form::text('address', null, array('placeholder' => __('customer.placeholder.address'),'class' => 'form-control')) !!}
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="fas fa-home"></span>
                                    </div>
                                </div>
                            </div>
                            @if ($errors->has('address'))
                            <span class="text-danger">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-12 mb-3">
                            <label for="contact"><strong>{{ __('customer.label.contact') }}</strong></label>
                            <div class="input-group">
                                {!! Form::text('contact', null, array('placeholder' => __('customer.placeholder.contact'),'class' => 'form-control')) !!}
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="far fa-address-book"></span>
                                    </div>
                                </div>
                            </div>
                            @if ($errors->has('contact'))
                            <span class="text-danger">{{ $errors->first('contact') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row align-items-center">
                        <label for="latitude"><strong>{{ __('Map Location') }}</strong></label>
                        <div class="card col-md-12 mb-3" id="map" style="height: 300px"></div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6 mb-3">
                            <label for="latitude"><strong>{{ __('customer.label.lat') }}</strong></label>
                            <div class="input-group">
                                {!! Form::text('latitude', null, array('placeholder' => __('customer.placeholder.lat'),'class' => 'form-control', 'id' => 'lat')) !!}
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="far fa-map"></span>
                                    </div>
                                </div>
                            </div>
                            @if ($errors->has('latitude'))
                            <span class="text-danger">{{ $errors->first('latitude') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="longitude"><strong>{{ __('customer.label.lng') }}</strong></label>
                            <div class="input-group">
                                {!! Form::text('longitude', null, array('placeholder' => __('customer.placeholder.lng'),'class' => 'form-control', 'id' => 'lng')) !!}
                                <div class="input-group-append">
                                    <div class="input-group-text">
                                        <span class="far fa-map"></span>
                                    </div>
                                </div>
                            </div>
                            @if ($errors->has('longitude'))
                            <span class="text-danger">{{ $errors->first('longitude') }}</span>
                            @endif
                        </div>
                        <div class="col-md-12 mb3">
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                {{ __('Untuk mengisi Latitude dan Longitude, cukup click pada peta di atas!')}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-sm btn-success"><i class="fas fa-user-plus"></i> &nbsp {{ __('customer.button.save') }}</button>
                <a class="btn btn-sm btn-secondary" href="{{ route('customers.index') }}"><i class="fa fa-arrow-left"></i> &nbsp {{ __('customer.button.back') }}</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    @section('custom_styles')
    <link href='https://api.mapbox.com/mapbox-gl-js/v3.0.1/mapbox-gl.css' rel='stylesheet' />
    @endsection

    @section('custom_scripts')
    <script src='https://api.mapbox.com/mapbox-gl-js/v3.0.1/mapbox-gl.js'></script>

    <script type="text/javascript">
        $(document).ready(function() {

            /*==================================================
                Server Dropdown Change Event
            ====================================================*/
            $('#server-dropdown').on('change', function() {
                var idServer = this.value;
                $("#paket-dropdown").html('');
                $.ajax({

                    url: "{{url('api/fetch-pakets')}}",
                    type: "POST",
                    data: {
                        mikrotik_id: idServer,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function(result_server) {
                        $('#paket-dropdown').html('<option value="">Select Paket</option>');
                        $.each(result_server.pakets, function(key, value) {
                            $("#paket-dropdown").append('<option value="' + value.id + '">' + value.name + ' - ' + 'Rp. ' + new Intl.NumberFormat().format(value.price) + ',00' + '</option>');
                        });
                    }

                });
            });
        });

        // mapbox
        mapboxgl.accessToken = 'pk.eyJ1Ijoicm9maWFyZWl2IiwiYSI6ImNsYW9xdHZ4cTB1OWYzcW1xaGVzZm84MGEifQ.xmNfOLtRRRWjk_skQzrR8A';
        const map = new mapboxgl.Map({
            container: 'map', // container ID
            style: 'mapbox://styles/mapbox/satellite-streets-v11', // style URL
            center: [113.42529, -7.04923], // starting position [lng, lat]
            zoom: 16, // starting zoom
        });

        const nav = new mapboxgl.NavigationControl({
            visualizePitch: true
        });

        map.addControl(nav, 'top-right');

        const lat = document.getElementById('lat');
        const lng = document.getElementById('lng');

        const marker = new mapboxgl.Marker();

        function add_marker (event) {
            var coordinates = event.lngLat;
            // console.log('Lng:', coordinates.lng, 'Lat:', coordinates.lat);
            marker.setLngLat(coordinates).addTo(map);
        }

        map.on('click', (e) => {
            let latLang = e.lngLat;
            // console.log(latLang);
            lat.value = latLang.lat;
            lng.value = latLang.lng;
            add_marker(e);
        });
    </script>
    @endsection
</x-layouts.cpanel.customers.app-layout>
