<x-layouts.cpanel.customers.app-layout title="Customers">
    <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary mb-2">
            {{ __('customer.header.index') }}
        </h5>

        <span>
            @if (isset($message))
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ $message }}
                </div>
            @else
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong> Total Incoming: @moneyIDR($totalIncoming) </strong><br>
                    {{ $data->total() }} customers found
                </div>
            @endif
        </span>
    </div>

    <div class="card-body">
        <div class="table-responsive">
            {{ $data->withQueryString()->links() }}
            <table id="customersTable" cellspacing="0" width="100%"
                class="table  datatable-loading no-footer sortable searchable table-responsive">
                <tr>
                    <th>{{ __('customer.table.no') }}</th>
                    <th>{{ __('customer.table.customer_number') }}</th>
                    <th>{{ __('customer.table.name') }}</th>
                    <th>{{ __('customer.table.address') }}</th>
                    <th>{{ __('customer.table.paket') }}</th>
                    <th>{{ __('customer.table.server') }}</th>
                    <th>{{ __('customer.table.state') }}</th>
                    <th width="80px">{{ __('customer.table.action') }}</th>
                </tr>
                @foreach ($data as $key => $customer)
                    <tr>
                        <td>{{ ++$i }}</td>
                        <td>
                            @can('customer-list')
                                <a class="dropdown-item text-primary"
                                    href="{{ route('customers.show', $customer->slug) }}">{{ $customer->customer_number }}</a>
                            @endcan
                        </td>
                        <td>
                            @can('customer-list')
                                <a class="dropdown-item text-primary"
                                    href="{{ route('customers.show', $customer->slug) }}">{{ $customer->name }}</a>
                            @else
                                {{ $customer->name }}
                            @endcan
                        </td>
                        <td>{{ $customer->address }}</td>
                        <td>
                            <span class="badge bg-danger-soft text-danger">
                                <i class="fas fa-chalkboard"></i> {{ $customer->paket->name }}
                            </span>
                        </td>
                        <td>
                            <span class="badge bg-danger-soft text-danger">
                                <i class="fas fa-server"></i> {{ $customer->mikrotik->name ?? 'None' }}
                            </span>
                        </td>

                        <td>
                            @if ($customer->activation == 'false')
                                <span class="badge text-white bg-danger">
                                    <i class="fa fa-times text-warning me-2"></i>
                                    {{ __('customer.info.not-activation') }}
                                </span>
                            @else
                                @if ($customer->disabled === \App\Enums\MikrotikStatus::PPPOE_ENABLE->value)
                                    <span class="badge text-white bg-success">
                                        <i class="fas fa-lock-open"></i> {{ __('pppoe.secret.enabled') }}
                                    </span>
                                @endif
                                @if ($customer->disabled === \App\Enums\MikrotikStatus::PPPOE_DISABLE->value)
                                    <span class="badge text-white bg-danger">
                                        <i class="fas fa-lock"></i> {{ __('pppoe.secret.disabled') }}
                                    </span>
                                @endif
                        </td>
                @endif
                <td class="d-flex items-center justify-content-end">
                    @canany(['secret-disable', 'customer-list', 'customer-edit', 'secret-edit', 'customer-delete'])
                        <x-drop-down-action>
                            @can('customer-edit')
                                <li>
                                    <a class="dropdown-item" href="{{ route('customers.edit', $customer->slug) }}">
                                        <i class="far fa-edit me-2 text-success"></i>
                                        {{ __('customer.action.edit') }}
                                    </a>
                                </li>
                                <li>
                                    @if ($customer->activation == 'false')
                                        <form method="POST" class="d-inline"
                                            action="{{ route('customers.activation', $customer->slug) }}">
                                            @csrf
                                            @method('put')
                                            <a class="dropdown-item" href="#"
                                                onclick="event.preventDefault(); this.closest('form').submit();">
                                                <i class="fa fa-check me-2 text-success"></i>
                                                {{ __('customer.action.activation') }}
                                            </a>
                                        </form>
                                    @else
                                        @if ($customer->disabled === \App\Enums\MikrotikStatus::PPPOE_DISABLE->value)
                                            <form method="POST" class="d-inline"
                                                action="{{ route('customers.enable', $customer->slug) }}">
                                                @csrf
                                                @method('put')
                                                <a class="dropdown-item" href="#"
                                                    onclick="event.preventDefault(); this.closest('form').submit();">
                                                    <i class="fa fa-check me-2 text-success"></i>
                                                    {{ __('pppoe.secret.enable') }}
                                                </a>
                                            </form>
                                            <form method="POST" class="d-inline"
                                                action="{{ route('customers.forceEnable', $customer->slug) }}">
                                                @csrf
                                                @method('put')
                                                <a class="dropdown-item" href="#"
                                                    onclick="event.preventDefault(); this.closest('form').submit();">
                                                    <i class="fa fa-check me-2 text-success"></i>
                                                    {{ __('pppoe.secret.force-enable') }}
                                                </a>
                                            </form>
                                        @elseif ($customer->disabled === \App\Enums\MikrotikStatus::PPPOE_ENABLE->value)
                                            <form method="POST" class="d-inline"
                                                action="{{ route('customers.disable', $customer->slug) }}">
                                                @csrf
                                                @method('put')
                                                <a class="dropdown-item" href="#"
                                                    onclick="event.preventDefault(); this.closest('form').submit();">
                                                    <i class="fa fa-times text-warning me-2"></i>
                                                    {{ __('pppoe.secret.disable') }}
                                                </a>
                                            </form>
                                            <form method="POST" class="d-inline"
                                                action="{{ route('customers.forceDisable', $customer->slug) }}">
                                                @csrf
                                                @method('put')
                                                <a class="dropdown-item" href="#"
                                                    onclick="event.preventDefault(); this.closest('form').submit();">
                                                    <i class="fa fa-times text-warning me-2"></i>
                                                    {{ __('pppoe.secret.force-disable') }}
                                                </a>
                                            </form>
                                        @endif
                                    @endif
                                </li>
                            @endcan
                            @can('customer-delete')
                                <li>
                                    <a class="dropdown-item" href="{{ route('customers.delete', $customer->slug) }}"><i
                                            class="fa fa-trash me-2 text-danger"></i>&nbsp{{ __('customer.action.delete') }}</a>
                                </li>
                            @endcan
                        </x-drop-down-action>
                    @endcanany
                </td>
                </tr>
                @endforeach
            </table>
            {{ $data->withQueryString()->links() }}
        </div>
    </div>

    @section('custom_scripts')
        <script type="text/javascript">
            $(document).ready(function() {

                /*==================================================
                    Server Dropdown Change Event
                ====================================================*/
                $('#server-dropdown').on('change', function() {
                    var idServer = this.value;
                    $("#paket-dropdown").html('');
                    $.ajax({

                        url: "{{ url('api/fetch-pakets') }}",
                        type: "POST",
                        data: {
                            mikrotik_id: idServer,
                            _token: '{{ csrf_token() }}'
                        },
                        dataType: 'json',
                        success: function(result_server) {
                            $('#paket-dropdown').html('<option value="">Select Paket</option>');
                            $.each(result_server.pakets, function(key, value) {
                                $("#paket-dropdown").append('<option value="' + value.id +
                                    '">' + value.name + ' - ' + 'Rp. ' + new Intl
                                    .NumberFormat().format(value.price) + ',00' +
                                    '</option>');
                            });
                        }

                    });
                });
            });
        </script>
    @endsection
</x-layouts.cpanel.customers.app-layout>
