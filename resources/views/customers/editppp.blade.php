<x-layouts.cpanel.customers.app-layout title="Edit PPP Secrets">

    <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary"> {{ __('customer.header.edit') }} - {{ $customer->name}}</h5>
    </div>

    <div class="row">
        <div class="col-lg-12">

            {!! Form::model($customer, ['method' => 'PATCH','route' => ['customers.update.ppp', $customer->slug]]) !!}
            <div class="card-body col-lg-8">

                <div class="row align-items-center">
                    <div class="col-md-12 mb-3">
                        <label for="paket"><strong>{{ __('customer.label.server') }}</strong>
                            <span class="badge bg-danger-soft text-danger">
                                <i class="fas fa-server"></i> {{ $customer->mikrotik->name }}
                            </span>
                        </label>
                    </div>
                </div>

                <div class="row align-items-center">
                    <div class="col-md-12 mb-3">
                        <label for="paket"><strong>{{ __('pppoe.secret.service') }}</strong>
                        </label>
                        {!! Form::select('ppp_type_id', $services, null, array('placeholder' => 'Select Service','class' => 'form-control', 'id' => 'server-dropdown')) !!}
                        @if ($errors->has('ppp_type_id'))
                        <span class="text-danger">{{ $errors->first('ppp_type_id') }}</span>
                        @endif
                    </div>
                </div>


                <div class="row align-items-center">
                    <div class="col-md-12 mb-3">
                        <label for="address"><strong>{{ __('customer.label.username') }}</strong></label>

                        {!! Form::text('username', null, array('placeholder' => __('customer.placeholder.username'),'class' => 'form-control')) !!}

                        @if ($errors->has('username'))
                        <span class="text-danger">{{ $errors->first('username') }}</span>
                        @endif
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-md-12 mb-3">
                        <label for="password"><strong>{{ __('customer.label.password-ppp') }}</strong></label>

                        {!! Form::text('password_ppp', null, array('placeholder' => __('customer.placeholder.username'),'class' => 'form-control')) !!}

                        @if ($errors->has('password_ppp'))
                        <span class="text-danger">{{ $errors->first('password_ppp') }}</span>
                        @endif
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-sm btn-success"><i class="fas fa-user-check"></i>&nbsp{{ __('customer.button.update-ppp') }}</button>
                <a class="btn btn-sm btn-secondary" href="{{ route('customers.edit', $customer->slug) }}"><i class="fa fa-arrow-left"></i>&nbsp{{ __('customer.button.cancel') }}</a>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</x-layouts.cpanel.customers.app-layout>