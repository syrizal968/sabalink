<x-layouts.cpanel.customers.app-layout title="Edit Customer">

    <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary"> {{ __('customer.header.edit') }}</h5>
    </div>

    <div class="row">
        <div class="col-lg-12">
            {!! Form::model($customer, ['method' => 'PATCH', 'route' => ['customers.update', $customer->slug]]) !!}
            <div class="card-body row">
                <div class="col-lg-6">
                    <div class="row align-items-center">
                        <div class="col-md-12 mb-3">
                            <label for="name"><strong>{{ __('customer.label.name') }}</strong></label>
                            {!! Form::text('name', null, ['placeholder' => __('customer.placeholder.name'), 'class' => 'form-control']) !!}
                            @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-12 mb-3">
                            <label for="paket"><strong>{{ __('customer.label.paket') }}</strong>
                            </label>
                            @if ($serverDisconnect === 'false')
                                <select name="paket_id" class="form-control dynamic" data-dependent="details">
                                    @foreach ($pakets as $paket)
                                        <option value="{{ $paket->id }}"
                                            {{ $paket->id == $customer->paket_id ? 'selected' : '' }}>
                                            {{ $paket->name }} - @moneyIDR($paket->price)</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('paket_id'))
                                    <span class="text-danger">{{ $errors->first('paket_id') }}</span>
                                @endif
                            @else
                                <input disabled type="text" value="{{ $customer->paket->name }}"
                                    class="form-control is-invalid"
                                    placeholder="{{ __('Server offline, please dont change it!') }}">
                                <input type="hidden" name="paket_id" value="{{ $customer->paket->name }}">
                                <span class="text-danger">{{ __('Server ') }} {{ $customer->mikrotik->name }}
                                    {{ __('Offline or Disable!') }}</span>
                            @endif

                        </div>
                    </div>
                    <div class="row align-items-center mb-3">
                        <div class="col-md-12">
                            <label for="discount"><strong>{{ __('customer.label.discount') }}</strong></label>
                        </div>
                        <div class="col-md-12 row">
                            <div class="col-md-2 mx-0">
                                {!! Form::text('discount', null, [
                                    'placeholder' => __('customer.placeholder.discount'),
                                    'class' => 'form-control',
                                ]) !!}
                            </div>
                            <div class=" py-2">
                                <strong>%</strong>
                            </div>
                        </div>
                        <div class="col-md-12">
                            @if ($errors->has('discount'))
                                <span class="text-danger">{{ $errors->first('discount') }}</span>
                            @endif
                        </div>

                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-6 mb-3">
                            <label for="email"><strong>{{ __('customer.label.email') }}</strong></label>
                            {!! Form::text('email', null, ['placeholder' => __('customer.placeholder.email'), 'class' => 'form-control']) !!}
                            @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="contact"><strong>{{ __('customer.label.contact') }}</strong></label>
                            {!! Form::text('contact', null, [
                                'placeholder' => __('customer.placeholder.contact'),
                                'class' => 'form-control',
                            ]) !!}
                            @if ($errors->has('contact'))
                                <span class="text-danger">{{ $errors->first('contact') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-md-6 mb-3">
                            <label for="password"><strong>{{ __('customer.label.password') }}</strong></label>
                            {!! Form::password('password', [
                                'placeholder' => __('customer.placeholder.password'),
                                'class' => 'form-control',
                            ]) !!}
                            @if ($errors->has('password'))
                                <span class="text-danger">{{ $errors->first('password') }}</span>
                            @endif
                        </div>
                        <div class="col-md-6 mb-3">
                            <label
                                for="confirm-password"><strong>{{ __('customer.label.confirm-password') }}</strong></label>
                            {!! Form::password('password_confirmation', [
                                'placeholder' => __('customer.placeholder.confirm-password'),
                                'class' => 'form-control',
                            ]) !!}
                            @if ($errors->has('password_confirmation'))
                                <span class="text-danger">{{ $errors->first('password_confirmation') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="row align-items-center">
                        <div class="col-md-12 mb-3">
                            <label for="address"><strong>{{ __('customer.label.address') }}</strong></label>
                            {!! Form::text('address', null, [
                                'placeholder' => __('customer.placeholder.address'),
                                'class' => 'form-control',
                            ]) !!}
                            @if ($errors->has('address'))
                                <span class="text-danger">{{ $errors->first('address') }}</span>
                            @endif
                        </div>
                    </div>

                    <div class="align-items-center">
                        <div class="col-md-12 mb-4 row">
                            <label
                                for="activation_date"><strong>{{ __('customer.label.activation-date') }}</strong></label>
                            @if ($serverDisconnect === 'false')
                                <div class="col-md-4">
                                    <input type="datetime-local" class="form-control" name="activation_date"
                                        value="{{ $customer->activation_date }}">
                                </div>
                                @if ($errors->has('activation_date'))
                                    <span class="text-danger">{{ $errors->first('activation_date') }}</span>
                                @endif
                            @else
                                <input disabled type="text" value="{{ $customer->activation_date }}"
                                    class="form-control">
                                <span class="text-danger">{{ __('Server ') }} {{ $customer->mikrotik->name }}
                                    {{ __('Offline or Disable!') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row align-items-center">
                        <label for="latitude"><strong>{{ __('Map Location') }}</strong></label>
                        <div class="card col-md-12 mb-3" id="map" style="height: 300px"></div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-md-12 mb-3">
                            <label for="latitude"><strong>{{ __('customer.label.lat') }}</strong></label>
                            {!! Form::text('latitude', null, [
                                'placeholder' => __('customer.placeholder.lat'),
                                'class' => 'form-control',
                                'id' => 'lat',
                            ]) !!}
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="longitude"><strong>{{ __('customer.label.lng') }}</strong></label>
                            {!! Form::text('longitude', null, [
                                'placeholder' => __('customer.placeholder.lng'),
                                'class' => 'form-control',
                                'id' => 'lng',
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-12 mb3">
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            {{ __('Untuk mengisi Latitude dan Longitude, cukup click pada peta di atas!') }}
                        </div>
                    </div>
                </div>

            </div>

            <div class="card-footer">
                <button type="submit" class="btn btn-sm btn-success"><i
                        class="fas fa-user-check"></i>&nbsp{{ __('customer.button.update') }}</button>
                <a class="btn btn-sm btn-primary" href="{{ route('customers.edit.router', $customer->slug) }}"><i
                        class="fas fa-user-edit"></i>&nbsp{{ __('customer.button.edit-router') }}</a>
                <a class="btn btn-sm btn-primary" href="{{ route('customers.edit.ppp', $customer->slug) }}"><i
                        class="fas fa-user-edit"></i>&nbsp{{ __('customer.button.edit-ppp') }}</a>
                <a class="btn btn-sm btn-secondary" href="{{ route('customers.index') }}"><i
                        class="fa fa-arrow-left"></i>&nbsp{{ __('customer.button.back') }}</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
    @section('custom_styles')
        <link href="{{ asset('css/flatpickr.min.css') }}" rel="stylesheet">
        <link href='https://api.mapbox.com/mapbox-gl-js/v3.0.1/mapbox-gl.css' rel='stylesheet' />
    @endsection
    @section('custom_scripts')
        <script src="{{ asset('js/flatpickr') }}"></script>

        <script src='https://api.mapbox.com/mapbox-gl-js/v3.0.1/mapbox-gl.js'></script>

        {{-- <script>
        config = {
            enableTime: false,
            dateFormat: 'd F Y',
        }
        flatpickr("input[type=datetime-local]", config);

        // mapbox
        mapboxgl.accessToken = 'pk.eyJ1Ijoicm9maWFyZWl2IiwiYSI6ImNsYW9xdHZ4cTB1OWYzcW1xaGVzZm84MGEifQ.xmNfOLtRRRWjk_skQzrR8A';
        const lat = document.getElementById('lat').value;
        const lng = document.getElementById('lng').value;
        // console.log(lat)
        const map = new mapboxgl.Map({
            container: 'map', // container ID
            style: 'mapbox://styles/mapbox/satellite-streets-v11', // style URL
            center: lat ? [lng, lat] : [113.42529, -7.04923], // starting position [lng, lat]
            zoom: 16, // starting zoom
        });


        const marker = new mapboxgl.Marker();

        if (lat) {
            marker.setLngLat([lng, lat])
                .addTo(map);
        }

        function add_marker (event) {
            var coordinates = event.lngLat;
            // console.log('Lng:', coordinates.lng, 'Lat:', coordinates.lat);
            marker.setLngLat(coordinates).addTo(map);
        }

        map.on('click', (e) => {
            let latLang = e.lngLat;
            // console.log(latLang);
            lat.value = latLang.lat;
            lng.value = latLang.lng;
            add_marker(e);
        });
    </script> --}}
        <script type="text/javascript">
            config = {
                enableTime: false,
                dateFormat: 'd F Y',
            }
            flatpickr("input[type=datetime-local]", config);

            $(document).ready(function() {

                /*==================================================
                    Server Dropdown Change Event
                ====================================================*/
                $('#server-dropdown').on('change', function() {
                    var idServer = this.value;
                    $("#paket-dropdown").html('');
                    $.ajax({

                        url: "{{ url('api/fetch-pakets') }}",
                        type: "POST",
                        data: {
                            mikrotik_id: idServer,
                            _token: '{{ csrf_token() }}'
                        },
                        dataType: 'json',
                        success: function(result_server) {
                            $('#paket-dropdown').html('<option value="">Select Paket</option>');
                            $.each(result_server.pakets, function(key, value) {
                                $("#paket-dropdown").append('<option value="' + value.id +
                                    '">' + value.name + ' - ' + 'Rp. ' + new Intl
                                    .NumberFormat().format(value.price) + ',00' +
                                    '</option>');
                            });
                        }

                    });
                });
            });

            // mapbox
            mapboxgl.accessToken =
                'pk.eyJ1Ijoicm9maWFyZWl2IiwiYSI6ImNsYW9xdHZ4cTB1OWYzcW1xaGVzZm84MGEifQ.xmNfOLtRRRWjk_skQzrR8A';
            const map = new mapboxgl.Map({
                container: 'map', // container ID
                style: 'mapbox://styles/mapbox/satellite-streets-v11', // style URL
                center: [113.42529, -7.04923], // starting position [lng, lat]
                zoom: 16, // starting zoom
            });

            const nav = new mapboxgl.NavigationControl({
                visualizePitch: true
            });

            map.addControl(nav, 'top-right');

            const lat = document.getElementById('lat');
            const lng = document.getElementById('lng');

            const marker = new mapboxgl.Marker();

            function add_marker(event) {
                var coordinates = event.lngLat;
                marker.setLngLat(coordinates).addTo(map);
            }

            // Set marker jika latitude dan longitude sudah ada
            if (lat.value && lng.value) {
                const existingCoordinates = {
                    lng: parseFloat(lng.value),
                    lat: parseFloat(lat.value)
                };
                marker.setLngLat(existingCoordinates).addTo(map);
                map.setCenter(existingCoordinates);
            }

            map.on('click', (e) => {
                let latLang = e.lngLat;
                lat.value = latLang.lat;
                lng.value = latLang.lng;
                add_marker(e);
            });
        </script>
    @endsection
</x-layouts.cpanel.customers.app-layout>
