<x-layouts.cpanel.billings.app-layout title="Billing Show">

    <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">{{ __('Billing Detail') }}</h5>
    </div>

    <div class="card-body col-lg-8">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Number:</strong>
                {{ $billing->billing_number ?? 'no data' }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $billing->customer_name ?? 'no data' }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Periode:</strong>
                {{ $billing->month ?? 'no data' }} - {{ $billing->year ?? 'no data' }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Paket:</strong>
                {{ $billing->paket_name ?? 'no data' }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Price:</strong>
                @if ($billing->paket_price === 0)
                {{ __('Free') }}
                @else
                @moneyIDR($billing->paket_price)
                @endif
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>User Created:</strong>
                <span class="badge bg-danger-soft text-danger">
                    <i class="fas fa-id-card-alt"></i> {{ $billing->created_name ?? 'no data' }}
                </span>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Status:</strong>
                @if ($billing->status === 'LS')
                <span class="badge bg-success text-white">
                    <i class="fas fa-check-circle"></i>
                    {{ __('billing.state.lunas') }}
                </span>
                {{ __('on ') }}
                <span class="badge bg-success text-white">
                    {{ Carbon\Carbon::parse($billing->payment_time)->translatedFormat('d F Y, H:i:s') }}
                </span>

                @elseif ($billing->status === 'PL')
                <span class="badge bg-warning text-white">
                    <i class="fas fa-ban"></i>
                    {{ __('billing.state.paylater') }}
                </span>
                @else
                <span class="badge bg-danger text-white">
                    <i class="fas fa-ban"></i>
                    {{ __('billing.state.belum-lunas') }}
                </span>
                @endif
            </div>
        </div>
        @if ($billing->status === 'LS' || $billing->status === 'PL')
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Teller:</strong>
                <span class="badge bg-danger-soft text-danger">
                    <i class="fas fa-id-card-alt"></i> {{ $billing->teller_name ?? 'No User' }}
                </span>
            </div>
        </div>
        @endif
        @if ($billing->status === 'PL')
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>{{ __('billing.pay-later') }}:</strong>
                <span class="badge bg-danger-soft text-danger">
                    <i class="far fa-bell"></i> {{ $billing->pay_later ?? 'No Data' }}
                </span>
            </div>
        </div>
        @endif
    </div>
    <div class="card-footer">
        @if ($billing->status === 'BL' || $billing->status === 'PL')
        {!! Form::model($billing, ['method' => 'PATCH','route' => ['billings.payment', $billing->slug],'style'=>'display:inline']) !!}
        <a href="#" class="btn btn-success btn-sm" onclick="event.preventDefault(); this.closest('form').submit();">
            <i class="fas fa-money-bill-wave text-white"></i>
            {{ __('billing.button.pay') }}
        </a>
        {!! Form::close() !!}
        @elseif ($billing->status === 'LS')
        {!! Form::model($billing, ['method' => 'PATCH','route' => ['billings.unpayment', $billing->slug],'style'=>'display:inline']) !!}
        <a href="#" class="btn btn-danger btn-sm" onclick="event.preventDefault(); this.closest('form').submit();">
            <i class="fas fa-money-bill-wave text-white"></i>
            {{ __('billing.button.unpay') }}
        </a>
        @endif
        <a class="btn btn-sm btn-primary" href="{{ URL::previous() }}"><i class="fa fa-arrow-left"></i> Back</a>
    </div>




    <div class="card-header py-3">
        <h5 class="mb-2 font-weight-bold text-primary">{{ __('Tunggakan Tagihan') }} - Total: @moneyIDR($totalPrice)</h5>
    </div>

    <div class="card-body">
        <table id="billingsTable" cellspacing="0" width="100%" class="table  datatable-loading no-footer sortable searchable">
            <tr>
                <th>{{ __('billing.table.no') }}</th>
                <th>{{ __('billing.table.periode') }}</th>
                <th>{{ __('billing.table.paket-name') }}</th>
                <th>{{ __('billing.table.paket-price') }}</th>
                <th>{{ __('billing.table.bill-state') }}</th>
                <th width="120px">{{ __('customer.table.action') }}</th>
            </tr>
            @foreach ($billings as $key => $billing)
            <tr>
                <td>{{$loop->iteration}}</td>

                <td><a href="{{ route('billings.show', $billing->slug) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="Show Billing">{{ $billing->month }} - {{ $billing->year }}</a></td>
                <td>
                    <span class="badge text-success">
                        <i class="fas fa-chalkboard"></i> {{ $billing->paket_name }}
                    </span>
                </td>
                <td>
                    <span class="badge bg-blue-soft text-primary">
                        @if ($billing->paket_price === 0)
                        {{ __('Free') }}
                        @else
                        @moneyIDR($billing->paket_price)
                        @endif

                    </span>
                </td>
                <td>
                    @if ($billing->status === 'LS')
                    <span class="badge bg-success text-white">
                        <i class="fas fa-check-circle"></i>
                        {{ __('billing.state.lunas') }}
                    </span>

                    @elseif ($billing->status === 'PL')
                    <span class="badge bg-warning text-white">
                        <i class="fas fa-ban"></i>
                        {{ __('billing.state.paylater') }}
                    </span>
                    @else
                    <span class="badge bg-danger text-white">
                        <i class="fas fa-ban"></i>
                        {{ __('billing.state.belum-lunas') }}
                    </span>
                    @endif
                </td>
                <td class="d-flex items-center justify-content-end">
                    {!! Form::model($billing, ['method' => 'PATCH','route' => ['billings.payment', $billing->slug]]) !!}
                    <a href="#" class="btn btn-success btn-sm" onclick="event.preventDefault(); this.closest('form').submit();">
                        <i class="fas fa-money-bill-wave text-white"></i>
                        {{ __('billing.button.pay') }}
                    </a>
                    {!! Form::close() !!}
                </td>
            </tr>
            @endforeach
        </table>
    </div>
</x-layouts.cpanel.billings.app-layout>