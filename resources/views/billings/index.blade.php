<x-layouts.cpanel.billings.app-layout title="Billings">

    <div class="card-header py-3">
        <h5 class="mb-2 font-weight-bold text-primary">{{ __('billing.header.index') }}</h5>
        <span>
            @can('billing-create')
                <a class="btn btn-success btn-sm" href="{{ route('billings.index', ['download' => 'pdf']) }}"
                    target="_blank"><i class="fa fa-print"></i> Cetak PDF</a>
            @endcan
        </span>
        @if (isset($message))
            <div class="alert alert-danger mt-2">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $message }}
            </div>
        @else
            <div class="alert alert-success mt-2">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{ $data->total() }} billings found
            </div>
        @endif
    </div>

    <div class="card-body">
        <div class="table-responsive">
            {{ $data->withQueryString()->links() }}
            @if ($totalPrice > 0)
                <div class="alert alert-success mt-2">
                    <strong> Total Bill: @moneyIDR($totalPrice) </strong>
                </div>
            @endif
            <table id="billingsTable" cellspacing="0" width="100%"
                class="table  datatable-loading no-footer sortable searchable">
                <tr>
                    <th>{{ __('billing.table.no') }}</th>
                    <th>{{ __('billing.table.customer-name') }}</th>
                    <th>{{ __('billing.table.address') }}</th>
                    <th>{{ __('billing.table.periode') }}</th>
                    <th>{{ __('billing.table.bill') }}</th>
                    <th>{{ __('billing.table.bill-state') }}</th>
                    {{-- <th>{{ __('Status Payments') }}</th> --}}
                    <th class="text-center">{{ __('Action') }}</th>
                </tr>
                @foreach ($data as $key => $billing)
                    <tr>
                        <td><span class="badge text-primary">{{ ++$i }}</span></td>

                        <td><a href="{{ route('billings.show', $billing->slug) }}" data-bs-toggle="tooltip"
                                data-bs-placement="top" title="Show Billing">
                                <span class="badge text-danger">{{ $billing->customer_name ?? 'no name' }}</span></a>
                        </td>
                        <td><span class="badge text-primary">{{ $billing->customer->address ?? 'no address' }}</span>
                        </td>
                        <td><span
                                class="badge text-primary">{{ $billing->billing_periode->name ?? 'no periode' }}</span>
                        </td>
                        <td>
                            <span class="badge bg-blue-soft text-danger">
                                @if ($billing->paket_price === 0)
                                    {{ __('Free') }}
                                @else
                                    @moneyIDR($billing->paket_price)
                                @endif
                            </span>
                        </td>
                        <td>
                            @if ($billing->status === 'LS')
                                <span class="badge bg-success text-white">
                                    <i class="fas fa-check-circle"></i>
                                    {{ __('billing.state.lunas') }}
                                </span>
                            @elseif ($billing->status === 'PL')
                                <span class="badge bg-warning text-white">
                                    <i class="fas fa-ban"></i>
                                    {{ __('billing.state.paylater') }}
                                </span>
                            @else
                                <span class="badge bg-danger text-white">
                                    <i class="fas fa-ban"></i>
                                    {{ __('billing.state.belum-lunas') }}
                                </span>
                            @endif
                        </td>
                        {{-- <td class="text-center"> --}}
                        {{-- @if ($billing->order->payment_status == 2)
                            <span class="badge bg-success text-white">
                                <i class="fas fa-check-circle"></i>
                                {{ __('billing.state.lunas') }}
                            </span>
                        @else
                            <span class="badge bg-danger text-white">
                                <i class="fas fa-ban"></i>
                                {{ __('billing.state.belum-lunas') }}
                            </span>
                        @endif --}}
                        {{-- </td> --}}
                        <td class="d-flex items-center justify-content-end">
                            @can('billing-edit')
                                @if ($billing->status === 'BL' || $billing->status === 'PL')
                                    @if ($billing->customer->activation === 'true')
                                        {!! Form::model($billing, ['method' => 'PATCH', 'route' => ['billings.payment', $billing->slug]]) !!}
                                        <a class="btn btn-success btn-sm"
                                            onclick="event.preventDefault(); this.closest('form').submit();">
                                            <i class="fas fa-money-bill-wave text-white"></i>
                                            {{ __('billing.button.pay') }}
                                        </a>
                                        {!! Form::close() !!}
                                        <a href="{{ route('billings.paylater', $billing->slug) }}"
                                            class="btn btn-success btn-sm ml-1">
                                            <i class="fas fa-money-bill-wave text-white"></i>
                                            {{ __('billing.button.pay-later') }}
                                        </a>
                                    @else
                                        <form method="POST" class="d-inline"
                                            action="{{ route('customers.activation', $billing->customer->slug) }}">
                                            @csrf
                                            @method('put')
                                            <a class="btn btn-success btn-sm"
                                                onclick="event.preventDefault(); this.closest('form').submit();">
                                                <i class="fa fa-check me-2 text-white"></i>
                                                {{ __('billing.button.pay') }}
                                            </a>
                                        </form>
                                    @endif
                                @elseif ($billing->status === 'LS')
                                    {!! Form::model($billing, ['method' => 'PATCH', 'route' => ['billings.unpayment', $billing->slug]]) !!}
                                    <a class="btn btn-danger btn-sm"
                                        onclick="event.preventDefault(); this.closest('form').submit();">
                                        <i class="fas fa-money-bill-wave text-white"></i>
                                        {{ __('billing.button.unpay') }}
                                    </a>
                                    {!! Form::close() !!}
                                @endif
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </table>
            {{ $data->withQueryString()->links() }}
        </div>
    </div>
</x-layouts.cpanel.billings.app-layout>
