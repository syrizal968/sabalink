<x-layouts.app-layout title=" {{ __('roles.title.index') }}">


    <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">{{ __('roles.header.index') }}</h5>
        <span><a class="btn btn-sm btn-success mt-2" href="{{ route('roles.create') }}"><i class="fa fa-plus"></i> {{ __('roles.button.create') }}</a></span>
    </div>

    <div class="card-body">
        <table id="rolesTable" cellspacing="0" width="100%" class="table  datatable-loading no-footer sortable searchable">

            <tr>
                <th width="40px">{{ __('roles.table.no') }}</th>
                <th>{{ __('roles.table.name') }}</th>
                <th>{{ __('roles.table.badge') }}</th>
                <th width="280px">{{ __('roles.table.action') }}</th>
            </tr>
            @foreach ($roles as $key => $role)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $role->name }}</td>
                <td><span class="badge bg-success text-white">{{ $role->name }}</span></td>
                <td>
                    @can('role-list')
                    <a href="{{ route('roles.show',$role->id) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="{{ __('roles.action.view') }}" class="btn btn-datatable btn-icon btn-transparent-dark me-2 router-show">
                        <i class="fas fa-external-link-alt text-success"></i>
                    </a>
                    @endcan
                    @can('role-edit')
                    <a href="{{ route('roles.edit',$role->id) }}" data-bs-toggle="tooltip" data-bs-placement="top" title="{{ __('roles.action.edit') }}" class="btn btn-datatable btn-icon btn-transparent-dark me-2 router-edit">
                        <i class="fas fa-edit text-primary"></i>
                    </a>
                    @endcan

                    @can('role-delete')
                    {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                    <button type="submit" class="btn text-danger btn-datatable btn-icon btn-transparent-dark me-2" data-bs-toggle="tooltip" data-bs-placement="top" title="{{ __('roles.action.delete') }}"><i class="far fa-trash-alt"></i></button>
                    {!! Form::close() !!}
                    @endcan

                </td>
            </tr>
            @endforeach
        </table>
        {!! $roles->render() !!}
    </div>

</x-layouts.app-layout>