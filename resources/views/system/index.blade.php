<x-layouts.app-layout title="System">
    <div class="card-header py-3">
        <h5 class="m-0 font-weight-bold text-primary">System</h5>
        <span><a class="btn btn-sm btn-success mt-2" href="{{ route('websystem.update', $data->id) }}"><i class="fas fa-edit"></i> Edit System Profile</a></span>
    </div>

    <div class="card-body row">
        <div class="col-md-6 mb-3">
            <div class="row align-items-center mb-2">
                <div class="col-md-4">
                    <label for="title">Title</label>
                </div>
                <div class="col-md-8">
                    {{ $data->title }}
                </div>
            </div>
            <div class="row align-items-center mb-2">
                <div class="col-md-4">
                    <label for="title">Address</label>
                </div>
                <div class="col-md-8">
                    {{ $data->address }}
                </div>
            </div>
            <div class="row align-items-center mb-2">
                <div class="col-md-4">
                    <label for="title">Website</label>
                </div>
                <div class="col-md-8">
                    {{ $data->website }}
                </div>
            </div>
            <div class="row align-items-center mb-2">
                <div class="col-md-4">
                    <label for="title">Phone</label>
                </div>
                <div class="col-md-8">
                    {{ $data->phone }}
                </div>
            </div>
            <div class="row align-items-center mb-2">
                <div class="col-md-4">
                    <label for="title">E-Mail</label>
                </div>
                <div class="col-md-8">
                    {{ $data->email }}
                </div>
            </div>
            <div class="row align-items-center mb-2">
                <div class="col-md-4">
                    <label for="title">Sistem Version</label>
                </div>
                <div class="col-md-8">
                    {{ $data->version }}
                </div>
            </div>
        </div>
        <div class="col-md-6 mb-3">
            <div class="row align-items-center mb-2">
                <div class="col-md-4">
                    <label for="title">Facebook</label>
                </div>
                <div class="col-md-8">
                    {{ $data->fb }}
                </div>
            </div>
            <div class="row align-items-center mb-2">
                <div class="col-md-4">
                    <label for="title">Intagram</label>
                </div>
                <div class="col-md-8">
                    {{ $data->ig }}
                </div>
            </div>
            <div class="row align-items-center mb-2">
                <div class="col-md-4">
                    <label for="title">Twitter</label>
                </div>
                <div class="col-md-8">
                    {{ $data->twitter }}
                </div>
            </div>
            <div class="row align-items-center mb-2">
                <div class="col-md-4">
                    <label for="title">Whatsapp</label>
                </div>
                <div class="col-md-8">
                    {{ $data->wa }}
                </div>
            </div>
        </div>
    </div>
    <div class="card-footer">
        <a class="btn btn-sm btn-secondary" onclick="window.history.back()" href="javascript:void(0)"><i class="fa fa-arrow-left"></i> &nbsp {{ __('Back') }}</a>
    </div>
</x-layouts.app-layout>
