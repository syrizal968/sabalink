<?php

return [

    'sidebar' => [
        'menu' => 'Menu',
        'dashboard' => 'Dashboard',
        'home' => 'Home',
        'customer' => 'Customer',
        'all_paket' => 'All Paket',
        'manage-customers' => 'Customers',
        'manage-roles' => 'Roles',
        'manage-users' => 'Users',
        'manage-account' => 'Account',
        'account' => [
            'profile' => 'Profile',
            'security' => 'Security',
        ],
        'setting' => 'Setting',
    ],

    'mikrotik' => [
        'sidebar' => [
            'server' => 'Server :mikrotik',
        ],

    ],

];
