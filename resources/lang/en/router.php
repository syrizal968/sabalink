<?php

return [
    'modem-type' => 'Modem',
    'server-type' => 'Server',
    'table' => [
        'no' => 'No',
        'merk' => 'Brand',
        'type' => 'Type',
        'server-type' => 'Server Type',
        'action' => 'Action',
    ],
    'title' => [],
    'header' => [
        'index' => 'Router',
        'create' => 'Create a new router',
        'index-brand' => 'Brand Router',
        'create-brand' => 'Add New Brand',
        'delete' => 'Delete router',
        'edit' => 'Edit Router',
        'edit-brand' => 'Edit Brand Router',
        'delete-brand' => 'Delete Brand router',
    ],
    'label' => [
        'brand' => 'Brand',
        'type' => 'Type',
        'server-type' => 'Server type?'

    ],
    'placeholder' => [
        'brand' => 'Please select brand',
        'type' => 'Type router',
        'name-brand' => 'Brand of router',

    ],
    'button' => [
        'create' => 'Add New Router',
        'create-brand' => 'Add New Brand',
        'save' => 'Save',
        'delete' => 'Delete',
        'cancel' => 'Cancel',
        'back' => 'Back',
        'show-brand-router' => 'Brand Router',

    ],


];
