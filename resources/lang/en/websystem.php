<?php

return [
    'title' => 'GriyaNet',
    'version' => 'Version: ',
    'new-version' => 'New Version: ',
    'header' => [
        'index' => 'Settings',
        'edit' => 'Change Setting',
    ],
    'label' => [
        'web-title' => 'Web Title:',
        'name-profile-isolir' => 'Name of Profile Isolir:',
        'comment-payment' => 'Comment Payment:',
        'comment-unpayment' => 'Comment Unpayment:',
        'due-date' => 'Due Date:'

    ],
    'info' => [
        'comment-payment' => 'This is used to mark user secrets who have paid on Mikrotik.',
        'comment-unpayment' => 'This is used to mark user secrets who have unpaid on Mikrotik.',
        'profile-isolir' => 'This is used to set a customer isolation profile.',
        'due-date' => 'Determine the payment date.',
        'every-due-date' => 'Every date :due_date',

    ],
    'button' => [
        'edit' => 'Edit',
        'update' => 'Update',
        'update-new-version' => 'Update to Version :version',
        'cancel' => 'Cancel',
    ],

    'midtran' => [
        'header-edit' => 'Configuration Midtrans',
        'name' => 'Name',
        'merchat_id' => 'Merchant Id',
        'server_key' => 'Server Key',
        'client_key' => 'Client Key',
        'sandbox_server_key' => 'Server Key Demo',
        'sandbox_client_key' => 'Client Key Demo',
        'production-mode' => 'Production Mode',
        'disable' => 'Disable',
        'placeholder' => [
            'name' => 'Name',
            'merchat_id' => 'Midtrans Merchant Id',
            'server_key' => 'Midtrans Server Key',
            'client_key' => 'Midtrans Client Key',
            'sandbox_server_key' => 'Midtrans Sandbox Server Key',
            'sandbox_client_key' => 'Midtrans Sandbox Client Key',
        ],
    ]


];
