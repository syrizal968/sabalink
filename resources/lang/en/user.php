<?php

return [

    'table' => [
        'no' => 'No',
        'name' => 'Name',
        'email' => 'Email',
        'roles' => 'Roles',
        'action' => 'Action',
    ],
    'title' => [
        'index' => 'Manage Users',
        'create' => 'Create New User',
        'edit' => 'Edit User',
        'show' => 'View User',
    ],
    'header' => [
        'index' => 'Users List',
        'create' => 'Create New User',
        'edit' => 'Edit User',
        'show' => 'User Information',
        'delete' => 'Delete User :user'
    ],
    'label' => [
        'name' => 'Name:',
        'email' => 'Email:',
        'password' => 'Password:',
        'confirm-password' => 'Confirm Password:',
        'roles' => 'Roles:',
    ],
    'placeholder' => [
        'name' => 'Insert your name',
        'email' => 'Insert your email:',
        'password' => 'Password:',
        'confirm-password' => 'Confirm your password',
    ],
    'button' => [
        'create' => 'Create a user',
        'edit' => 'Edit user',
        'save' => 'Add User',
        'update' => 'Update Data User',
        'delete' => 'Delete User',
        'back' => 'Back',
        'cancel' => 'Cancel',
    ],

    'action' => [
        'view' => 'View this user',
        'edit' => 'Edit this user',
        'delete' => 'Delete this user',
    ],

    'info' => [
        'whoops' => 'Whoops!',
        'there_were_some_problems_with_your_input' => 'There were some problems with your input.',
        'disabled' => 'Disable',
        'enabled' => 'Enable',
        'are_you_sure_to_delete_user' => 'Are you sure to delete user :user?',
        'after-delete' => 'After delete, it cannot be recovered.',
        'press-button' => 'Press :button-delete button to delete user :user and press :button-cancel to cancel this operation.',
    ],


];
