<?php

return [

    'table' => [
        'no' => 'No',
        'name' => 'Name',
    ],
    'title' => [],
    'header' => [
        'delete' => 'Delete Server :server',
        'server-offline' => 'Server Offline: :server',
    ],
    'label' => [
        'name' => 'Name',

    ],
    'placeholder' => [
        'name' => 'Insert server name',

    ],
    'button' => [
        'delete' => 'Delete',
        'cancel' => 'Cancel',

    ],
    'info' => [
        'are_you_sure_to_delete_server' => 'Are you sure to delete server :server ?',
        'warning-delete' => 'Warning!!! Deleting a server :server will delete their all member and all pakets on server :server',
        'after-delete' => 'After delete, it cannot be recovered.',
        'press-button' => 'Press :button-delete button to delete server :server and press :button-cancel to cancel this operation.'
    ]


];
