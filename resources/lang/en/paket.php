<?php

return [

    'table' => [
        'no' => 'No',
        'name' => 'Name',
        'price' => 'Price',
        'server' => 'Server',
        'profile' => 'Profile',
        'description' => 'Description',
        'action' => 'Action',
        'status' => 'Status',
    ],
    'title' => [
        'index' => 'Paket Management',
        'create' => 'Create New Paket',
        'edit' => 'Edit Paket',
        'show' => 'View Paket',
    ],
    'header' => [
        'index' => 'Paket Management',
        'create' => 'Create New Paket',
        'edit' => 'Edit Paket',
        'show' => 'Paket Information',
        'export' => 'Export Profile to Router',
        'delete' => 'Delete Paket',
    ],
    'label' => [
        'name' => 'Name:',
        'profile' => 'Profile:',
        'price' => 'Price:',
        'description' => 'Description:',
    ],
    'placeholder' => [
        'name' => 'Insert name',
        'price' => 'Price of this paket',
        'description' => 'Description of this paket',
    ],
    'button' => [
        'create' => 'Create new Paket',
        'edit' => 'Edit Paket',
        'save' => 'Add Paket',
        'update' => ' Update Paket',
        'delete' => 'Delete Paket',
        'back' => ' Back',
        'cancel' => ' Cancel',
    ],

    'action' => [
        'show' => 'View this paket',
        'edit' => 'Edit this paket',
        'delete' => 'Delete this paket',
        'enable' => 'Enable',
        'disable' => 'Disable',
    ],

    'info' => [
        'whoops' => 'Whoops!',
        'there_were_some_problems_with_your_input' => 'There were some problems with your input.',
        'enabled' => 'Enable',
        'disabled' => 'Disable',
    ],


];
