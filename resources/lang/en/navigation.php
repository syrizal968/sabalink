<?php

return [
    'customers' => [
        'nav-item' => 'Customers',
        'index' => 'Show All Customers',
        'show-offline' => 'Offline Customers',
        'create' => 'Add New Customer',
    ],

    'billings' => [
        'nav-item' => 'Billings',
        'index' => 'Show All Bills',
        'create' => 'Create New Bill',
        'add' => 'Add New Bill',
        'show' => 'All Bill',
        'show-paid' => 'Paid Bill',
        'show-unpaid' => 'Unpaid Bill',
        'show-paylater' => 'Pay Later',
    ],
    'pakets' => [
        'nav-item' => 'Packages',
        'index' => 'Show All Packages',
        'create' => 'Create New Package',
    ],

];
