<?php

return [
    'pay-later' => 'Pay Later',
    'table' => [
        'no' => 'No',
        'number' => 'Billing No.',
        'customer-name' => 'Name',
        'periode' => 'Periode',
        'address' => 'Address',
        'paket-name' => 'Paket',
        'bill' => 'Bill',
        'bill-state' => 'Status',
        'paket-price' => 'Price',
        'action' => 'Action',

    ],
    'placeholder' => [
        'customer-name' => 'Customer Name',
        'select-month' => 'Select Month',
        'select-year' => 'Select Year',
        'select' => [
            'status' => 'Status',
            'paid' => 'Lunas',
            'unpaid' => 'Belum Bayar',
            'periode' => 'Please select periode',
            'paylater' => 'Bayar Nanti',
        ],
    ],
    'title' => [
        'index' => 'Manage Billings',
        'create' => 'Create New Billing',
        'edit' => 'Edit Billing',
        'show' => 'View Billing',
        'create-one' => 'Add New Billing',

    ],
    'header' => [
        'index' => 'Billings List',
        'create' => 'Add New Billing',
        'create-one' => 'Add Billing to Existing Periode',
        'edit' => 'Edit Billing',
        'show' => 'Billing Detail',
        'paid-off' => 'Lunas',
        'search' => 'Search',
        'delete' => 'Delete billing periode :billingperiode',
    ],
    'label' => [
        'periode' => 'Periode:',

    ],

    'button' => [
        'created-new-bill' => 'Create New Bills',
        'add-new-bill' => 'Add New Bill',
        'edit' => 'Edit Bill',
        'save-add' => 'Add Bills',
        'save-create' => 'Create Bills',
        'update' => 'Update Bill',
        'delete' => 'Delete Bill',
        'back' => 'Back',
        'cancel' => 'Cancel',
        'pay' => 'Pay',
        'unpay' => 'Unpay',
        'search' => 'Search Bills',
        'view' => 'Show',
        'all-payment' => 'All',
        'waiting-payment' => 'Menunggu Pembayaran',
        'paid-off' => 'Lunas',
        'expired' => 'Kadaluarsa',
        'cancelled' => 'Di Batalkan',
        'unpaid' => 'Belum bayar',
        'create-order' => 'Order',
        'view-order' => 'View Order',
        'pay-later' => 'Pay Later',
    ],

    'action' => [
        'view' => 'View this billing',
        'edit' => 'Edit this billing',
        'delete' => 'Delete this billing',
    ],

    'info' => [
        'whoops' => 'Whoops!',
        'there_were_some_problems_with_your_input' => 'There were some problems with your input.',
        'are_you_sure_to_delete_billing' => 'Are you sure to delete billing periode :billingperiode?',
        'warning-delete' => 'Warning!!! Deleting a billing will delete their all bill in periode :billingperiode.',
        'after-delete' => 'After delete, it cannot be recovered.',
        'press-button' => 'Press :button-delete button to delete billing periode :billingperiode and press :button-cancel to cancel this operation.',
    ],
    'state' => [
        'belum-lunas' => 'Has not paid off',
        'Paid Off' => 'Lunas',
        'paylater' => 'Pay Later',
    ],
    'status' => [
        'waiting-payment' => 'Waiting Payment',
        'paid-off' => 'Lunas',
        'expired' => 'kadaluarsa',
        'cancelled' => 'dibatalkan',
    ],
    'mikrotik' => [
        'comment-belum-lunas' => 'belum_lunas',
        'comment-lunas' => 'lunas',
        'profile_isolir' => 'profile_isolir',
    ]


];
