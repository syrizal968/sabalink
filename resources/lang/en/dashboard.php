<?php

return [
    'dashboard' => 'Dashboard',
    'users' => 'Users',
    'welcome-user' => 'Hi, :user',
    'new-router' => 'New router',
    'total-router' => 'Total router',
    'connected' => 'Connected',
    'company' => 'GriyaNet',
    'router-list' => 'Router list',
    'total-client' => 'Total client',
    'bill-ammount' => 'Bill ammount',
    'paid' => 'Paid',

];
