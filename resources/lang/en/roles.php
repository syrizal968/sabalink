<?php

return [

    'table' => [
        'no' => 'No',
        'name' => 'Name',
        'badge' => 'Badge',
        'action' => 'Action',
    ],
    'title' => [
        'index' => 'Role Management',
        'create' => 'Create New Role',
        'edit' => 'Edit Role',
        'show' => 'View Role',
    ],
    'header' => [
        'index' => 'Role Management',
        'create' => 'Create New Role',
        'edit' => 'Edit Role',
        'show' => 'Role Information',
    ],
    'label' => [
        'name' => 'Name:',
        'permissions' => 'Permissions:',
    ],
    'placeholder' => [
        'name' => 'Insert name',
    ],
    'button' => [
        'create' => 'Create new role',
        'edit' => 'Edit Role',
        'save' => 'Add role',
        'update' => ' Update Role',
        'delete' => 'Delete Role',
        'back' => ' Back',
        'cancel' => ' Cancel',
    ],

    'action' => [
        'view' => 'View this role',
        'edit' => 'Edit this role',
        'delete' => 'Delete this role',
    ],

    'info' => [
        'whoops' => 'Whoops!',
        'there_were_some_problems_with_your_input' => 'There were some problems with your input.',
    ],
    'permission' => [
        'role' => 'Role',
        'paket' => 'Paket',
        'ppp' => 'PPPoE',
        'user' => 'User',
        'customer' => 'Customer',
        'server' => 'Server',
    ],


];
