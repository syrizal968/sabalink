<?php

return [
    'dashboard-router' => 'Dashboard router: :name',
    'date-time' => 'Date & Time',


    'secret' => [
        'title' => 'PPPoE Secrets',
        'add-secret' => 'Add Secret',
        'edit-secret' => 'Edit Secret',
        'list-secret-on' => 'List secrets on: :name',
        // 'server-on' => 'Server on: :name',
        'server-on' => 'Server on:',
        'all-secret' => 'All secret',
        'name' => 'Name',
        'password' => 'Password',
        'profile' => 'Profile',
        'local-addr' => 'Local Addr',
        'remote-addr' => 'Remote Addr',
        'last-offline' => 'Last Ofline',
        'state' => 'Status',
        'actions' => 'Actions',
        'disabled' => 'Disabled',
        'enabled' => 'Enabled',
        'disable' => 'Disable',
        'enable' => 'Enable',
        'delete' => 'Delete',
        'force-enable' => 'Force Enable',
        'force-disable' => 'Force Disable',
        'edit' => 'Edit',
        'name' => 'Name',
        'password' => 'Password',
        'service' => 'Service',
        'profile' => 'Profile',
        'button' => [
            'back' => 'Back',
            'cancel' => 'Cancel',
            'save' => 'Add User Secret',
            'update' => 'Update User Scret',
            'copy' => 'Import Customer',
        ],
    ],
    'profile' => [
        'name' => 'Profile Name on Your Mikrotik',
        'rate-limit' => 'Rate Limit (rx/tx)',
        'button' => [
            'copy' => 'Import Profile to Customer Management',
        ],
    ],
];
