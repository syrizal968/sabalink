<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('pakets')) {
            Schema::create('pakets', function (Blueprint $table) {
                $table->id();
                $table->foreignId('mikrotik_id')->references('id')->on('mikrotiks')->onDelete('cascade');
                $table->string('name');
                $table->string('profile');
                $table->string('slug')->unique();
                $table->text('description')->nullable();
                $table->integer('price');
                //Add new disable
                $table->enum('disabled', ['false', 'true'])->default('true');
                //end
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pakets', 'customers');
        // Schema::dropIfExists('customers');
    }
};
