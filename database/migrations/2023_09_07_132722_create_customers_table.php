<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('customers')) {
            Schema::create('customers', function (Blueprint $table) {
                $table->id();
                // $table->foreignId('user_id')->references('id')->on('users')->onDelete('cascade');
                $table->string('name');
                $table->string('slug')->unique()->nullable();
                $table->string('password');
                $table->string('email')->unique();
                $table->foreignId('paket_id')->unsigned()->nullable();
                $table->foreignId('mikrotik_id')->unsigned()->nullable();
                $table->string('secret_id')->nullable();
                $table->string('username')->nullable();
                $table->string('password_ppp')->nullable();
                $table->foreignId('ppp_type_id')->unsigned()->nullable();
                $table->text('address')->nullable();
                $table->text('contact')->nullable();
                //Add new disable
                $table->enum('disabled', ['false', 'true'])->default('true')->nullable();
                $table->enum('activation', ['false', 'true'])->default('false')->nullable();
                $table->string('activation_date')->nullable();
                //end
                $table->timestamps();

                $table->foreign('paket_id')->references('id')->on('pakets')->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('mikrotik_id')->references('id')->on('mikrotiks')->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('ppp_type_id')->references('id')->on('ppp_types')->onUpdate('cascade')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('customers', function (Blueprint $table) {
            //
        });
    }
};
