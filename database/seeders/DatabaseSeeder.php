<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Database\Seeders\WebsystemSeeder;
use Database\Seeders\CreatePppTypeSeeder;
use Database\Seeders\CreateAdminUserSeeder;
use Database\Seeders\PermissionTableSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->call(PermissionTableSeeder::class);
        $this->call(CreatePppTypeSeeder::class);
        $this->call(CreateAdminUserSeeder::class);
        $this->call(WebsystemSeeder::class);
    }
}
