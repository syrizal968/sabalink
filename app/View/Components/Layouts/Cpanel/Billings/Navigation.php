<?php

namespace App\View\Components\Layouts\Cpanel\Billings;

use Closure;
use App\Models\Customer;
use Illuminate\View\Component;
use App\Models\Billing\Billing;
use Illuminate\Contracts\View\View;

class Navigation extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        $customers = Customer::orderBy('address', 'ASC')->where('address', '!=', NULL)->get();
        $addressUnique = $customers->unique('address');
        $address = $addressUnique->pluck('address', 'address');
        $years = Billing::orderBy('year', 'DESC')->pluck('year', 'year');
        // $months = Billing::orderBy('month', 'DESC')->pluck('month', 'month');
        return view('components.layouts.cpanel.billings.navigation', compact('years', 'address'));
    }
}
