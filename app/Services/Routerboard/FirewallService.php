<?php

namespace App\Services\Routerboard;

use App\Http\Resources\TrafficInterfaceResource;
use App\Models\Mikrotik;
use Exception;
use Illuminate\Http\JsonResponse;
use RouterOS\Client;
use RouterOS\Config;
use RouterOS\Exceptions\BadCredentialsException;
use RouterOS\Exceptions\ClientException;
use RouterOS\Exceptions\ConfigException;
use RouterOS\Exceptions\ConnectException;
use RouterOS\Exceptions\QueryException;
use RouterOS\Query;

class FirewallService
{
    /**
     * @throws ClientException
     * @throws ConnectException
     * @throws QueryException
     * @throws BadCredentialsException
     * @throws ConfigException
     * @throws Exception
     */
    public function getMikrotik($routerboard): Client
    {
        $mikrotik = $routerboard;
        if (!$mikrotik) {
            throw new Exception('Mikrotik not found.');
        }

        $config = (new Config())
            ->set('host', $mikrotik->host)
            ->set('port', (int) $mikrotik->port)
            ->set('pass', $mikrotik->password)
            ->set('user', $mikrotik->username);

        return new Client($config);
    }

    /**
     * @throws ClientException
     * @throws ConnectException
     * @throws QueryException
     * @throws BadCredentialsException
     * @throws ConfigException
     */
    public function getIpAddressList($routerboard)
    {
        $client = $this->getMikrotik($routerboard);
        $query = (new query('/ip/firewall/address-list/print'))
            ->where('disabled', 'false');
        return $client->query($query)->read();
    }

    public function createNatService($routerboard, $dstAddress, $srcAddressList, $dstAddressList)
    {
        $client = $this->getMikrotik($routerboard);
        $query = (new Query('/ip/firewall/nat/add'))
            ->equal('chain', 'dstnat')
            ->equal('action', 'redirect')
            ->equal('to-ports', '8080')
            ->equal('protocol', 'tcp')
            ->equal('dst-address', '!' . $dstAddress)
            ->equal('src-address-list', $srcAddressList)
            ->equal('dst-address-list', $dstAddressList)
            ->equal('dst-port', '80,443')
            ->equal('comment', 'Create from customer management to redirect isolir profile');
        return $client->query($query)->read();
    }

    public function updateNatService($routerboard, $dstAddress, $srcAddressList, $dstAddressList, $id)
    {
        $client = $this->getMikrotik($routerboard);
        $natService = new Query('/ip/firewall/nat/print');
        $natService->where('.id', $id);
        $results = $client->query($natService)->read();

        foreach ($results as $result) {
            $client = $this->getMikrotik($routerboard);
            $query = (new Query('/ip/firewall/nat/set'))
                ->where('.id', $id)
                ->equal('.id', $result['.id'])
                ->equal('dst-address', '!' . $dstAddress)
                ->equal('src-address-list', $srcAddressList)
                ->equal('dst-address-list', $dstAddressList);

            return $client->query($query)->read();
        }
    }

    public function removeNatSevice($routerboard, $id)
    {
        $client = $this->getMikrotik($routerboard);
        $natService = new Query('/ip/firewall/nat/print');
        $natService->where('.id', $id);
        $results = $client->query($natService)->read();

        foreach ($results as $result) {
            $client = $this->getMikrotik($routerboard);
            $query = (new Query('/ip/firewall/nat/remove'))
                ->where('.id', $id)
                ->equal('.id', $result['.id']);

            return $client->query($query)->read();
        }
    }
}
