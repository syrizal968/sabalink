<?php

namespace App\Http\Controllers\Midtran;

use App\Models\Order;
use App\Models\Midtran;
use App\Models\Websystem;
use Illuminate\Http\Request;
use App\Models\Billing\Billing;
use App\Http\Controllers\Controller;
use App\Services\Midtrans\CreateSnapTokenService;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('customerauth');
    }
    public function index()
    {

        $paymentGateway = Websystem::where('slug', 'websystem')->value('payment_gateway');
        if ($paymentGateway == 'enable') {
            $customerId = (auth()->guard('customer')->user()->id);
            $state = request('status');
            $orders = Order::where('customer_id', $customerId)
                ->when($state, function ($query) use ($state) {
                    $query->where('payment_status', $state);
                })
                ->orderBy('created_at', 'ASC')
                ->paginate(10);
            return view('customers.pages.orders.index', compact('orders'))
                ->with('i', (request()->input('page', 1) - 1) * 10);
        } else {
            return redirect()->route('customer.dashboard')
                ->with('error', 'You canot use payment gateway');
        }
    }

    public function paymentSuccess(Request $request)
{
    $order = Order::where('number', $request->order_id)->first();
    if ($order) {
        $order->payment_status = 2; // Update status pembayaran menjadi 'sudah dibayar'
        $order->transaction_id = $request->transaction_id;
        $order->payment_type = $request->payment_type;
        $order->payment_time = $request->payment_time;
        $order->pdf_url = $request->pdf_url;
        $order->save();

        $billing = Billing::find($order->billing_id);
        if ($billing) {
            $billing->status = 'LS'; // Ganti dengan status yang sesuai (misalnya 'LS' untuk lunas)
            $billing->save();
        }
    }

    return response()->json(['status' => 'success']);
}


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Billing $billing)
    {
        $paymentGateway = Websystem::where('slug', 'websystem')->value('payment_gateway');
        if ($paymentGateway == 'enable') {
            $order = Order::create([
                'customer_id' => $billing->customer_id,
                'billing_id' => $billing->id,
                'number' => rand(10000000, 99999999) . str_pad($billing->customer_id . $billing->id,  5, STR_PAD_LEFT),
                'total_price' => $billing->paket_price,
                'payment_status' => 1,
            ]);
            $getOrder = $order->fresh();

            return redirect()->route('customer.order.show', $getOrder->id)
                ->with('success', 'Order created successfully.');
        } else {
            return redirect()->route('customer.dashboard')
                ->with('error', 'You canot use payment gateway');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $paymentGateway = Websystem::where('slug', 'websystem')->value('payment_gateway');
        if ($paymentGateway == 'enable') {
            $midtrans = Midtran::where('slug', 'midtrans')->first();
            if ($midtrans->midtrans_is_production == 'false') {
                $clientKey = $midtrans->midtrans_sandbox_client_key;
                $midtransJs = 'https://app.sandbox.midtrans.com/snap/snap.js';
            } else {
                $clientKey = $midtrans->midtrans_client_key;
                $midtransJs = 'hhttps://app.midtrans.com/snap/snap.js';
            }
            $snapToken = $order->snap_token;
            if (is_null($snapToken)) {
                $midtrans = new CreateSnapTokenService($order);
                $snapToken = $midtrans->getSnapToken();
                $order->snap_token = $snapToken;
                $order->save();
            }

            return view('customers.pages.orders.show', compact('order', 'snapToken', 'clientKey', 'midtransJs'));
        } else {
            return redirect()->route('customer.dashboard')
                ->with('error', 'You canot use payment gateway');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
