<?php

namespace App\Http\Controllers;

use App\Models\WebSystem;
use Illuminate\Http\Request;

class WebsystemController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = WebSystem::first();

        return view('system.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(WebSystem $webSystem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(WebSystem $webSystem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, WebSystem $webSystem)
    {
        if ($webSystem->payment_gateway == 'disable') {
            $status = 'enable';
            $messages = 'Payment gateway enable successfully.';
        } else {
            $status = 'disable';
            $messages = 'Payment gateway disable successfully.';
        }
        $webSystem->update([
            'payment_gateway' => $status,
        ]);
        return redirect()->back()
            ->with('success', $messages);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(WebSystem $webSystem)
    {
        //
    }
}
