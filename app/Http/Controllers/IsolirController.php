<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\Mikrotik;
use Illuminate\Http\Request;
use App\Http\Resources\Pppoe\SecretResource;
use App\Models\Billing\Billing;
use App\Services\Routerboard\Ppp\SecretService;
use App\Services\Routerboard\TestConnectionService;

class IsolirController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    private SecretService $secretService;
    private TestConnectionService $testConnectionService;
    public function __construct(SecretService $secretService, TestConnectionService $testConnectionService)
    {
        $this->secretService = $secretService;
        $this->testConnectionService = $testConnectionService;
    }
    public function index()
    {
        $clientIP = $_SERVER['REMOTE_ADDR'];
        //$clientIP = '10.1.0.3';
        $routers = Mikrotik::all();
        foreach ($routers as $router) {
            try {
                // $this->testConnectionService->checkOnline($router);
                $getSecret = $this->secretService->getPppSecretActiveByIp($router, $clientIP);
                $secret = SecretResource::collection($getSecret)->first();
                if ($getSecret) {
                    $username = $secret['name'];
                    $routerId = $router->id;
                    $customer = Customer::where('username', $username)->where('mikrotik_id', $routerId)->first();
                    $billings = Billing::where('customer_id', $customer->id)
                        ->where('status', '!=', 'LS')
                        ->get();

                    if ($billings->first()) {
                        return view('isolir.isolir_customer', compact('customer', 'billings'));
                    } else {
                        return redirect()->route('customer.dashboard');
                    }
                }
            } catch (\Exception $e) {
                // return redirect()->back()->withErrors(['msg' => 'Router offline, please check your router.']);
            }
        }
        return redirect()->route('customer.dashboard');
    }
}
