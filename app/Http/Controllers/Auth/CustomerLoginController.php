<?php

namespace App\Http\Controllers\Auth;

use App\Models\Paket;
use App\Models\PppType;
//use Illuminate\Support\Facades\Auth;
//use Illuminate\Http\RedirectResponse;
use App\Models\Customer;
use App\Models\AutoIsolir;
use App\Models\CustomerOnt;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Billing\Billing;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Models\Billing\BillingPeriode;
use App\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class CustomerLoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::CUSTOMERHOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //public function __construct()
    // {
    //    $this->middleware('guest')->except('logout');
    //}

    public function login()
    {

        return view('customers.auth.login');
    }

    public function postLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

    //     if (auth()->guard('customer')->attempt(['email' => $request->input('email'),'password' => $request->input('password')]))  {
    //         $user = auth()->guard('customer')->user();
    //         return redirect()->route('customer.dashboard')
    //             ->withSuccess('You have Successfully loggedin');
    //         return dd($user);
    //         if ($user->is_customer == 1) {
    //         return redirect()->route('customerDashboard')->with('success', 'You are Logged in sucessfully.');
    //         }
    //     } else {
    //         $request->session()->invalidate();
    //         return back()->with('error', ['Whoops! invalid email and password.', 'Your Account is suspended, please contact Admin.']);
    //     }
    // }
            if (auth()->guard('customer')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
                $user = auth()->guard('customer')->user();

                // Cek apakah akun pengguna dinonaktifkan
                if ($user->disabled == 'false') {
                    return redirect()->route('customer.dashboard')
                        ->withSuccess('Anda berhasil login.');
                } else {
                    auth()->guard('customer')->logout();
                    return back()->with('error', 'Akun Anda dinonaktifkan, silahkan hubungi Admin.');
                }
            } else {
                return back()->with('error', 'Email atau password salah.');
            }
}

    public function register()
    {
        $paket = Paket::select('id', 'name', 'price')->get();
       
        return view('customers.auth.register',['paket' => $paket]);
    }

    public function store(Request $request)
    // {
        // $slug = Str::slug($request->title, '-');
        // $request->request->add(['slug' => str($newNumber . '_' . $first_name)->slug()]);
        // Customer::create([
        //     'id' => $request->id,
        //     'name' => $request->name,
        //     'email' => $request->email,
        //     'slug' => $request->slug,
        //     'password' => $request->password,
        //     'contact' => $request->contact,
        //     'address' => $request->address,
    
        // ]);
        {
            // if ($request->discount) {
            //     $discount = $request->discount;
            // } else {
            //     $discount = 0;
            // }
            $mikrotikId = 1;
            $date = Carbon::now()->format('d');
            if ($date >= 29) {
                $date = 28;
            }
            $periodeId = BillingPeriode::latest('created_at')->value('id');
            if (!isset($periodeId)) {
                $billingPeriode = BillingPeriode::create([
                    'name' => Carbon::now()->format('F Y'),
                ]);
                $newBillingPeriode = $billingPeriode->fresh();
                $periodeId = $newBillingPeriode->id;
                $periodeName = $newBillingPeriode->name;
            } else {
                $periodeName = BillingPeriode::latest('created_at')->value('name');
            }
            $month = Carbon::parse($periodeName)->format('m');
            $year = Carbon::parse($periodeName)->format('Y');
    
            $name = explode(' ', $request->name);
            $first_name = $name[0];
            //Informasi ppp
             // ambil data pelanggan terakhir
             $lastCustomer = Customer::latest()->first();
             if ($lastCustomer->customer_number !== null) {
                 // buat nomor pelanggan
                 $newNumber = $lastCustomer->id + 1 . Carbon::now()->format('Ymd');
             } else {
                 $newNumber = $lastCustomer->id + 1 . Carbon::now()->format('Ymd');
             }
            $username = $newNumber . '_' . strtoupper($first_name);
            $passwordPpp = '12345';
    
            // $mikrotikId = $request->mikrotik_id;
            // $router = Mikrotik::where('id', $mikrotikId)->first();
    
            // $autoIsolir = AutoIsolir::where('mikrotik_id', $mikrotikId)->first();
            // if (isset($autoIsolir)) {
            //     if ($autoIsolir->activation_date == 'true') {
            //         $comment_unpayment = $autoIsolir->comment_unpayment . '_' . $date . '_' . $month . '_' . $year;
            //     } else {
            //         $comment_unpayment = $autoIsolir->comment_unpayment;
            //     }
            //     $firstProfile = $autoIsolir->profile_id;
            // } else {
            //     $firstProfile = Paket::where('mikrotik_id', $mikrotikId)->where('id', $request->paket_id)->value('profile');
            //     $comment_unpayment = 'create_from_customer_management';
            // }
            // $serviceName = PppType::where('id', $request->ppp_type_id)->value('name');
    
            // try {
    
            //     $createSecret = $this->secretService->createSecret($router, $username, $passwordPpp, $serviceName, $firstProfile, $comment_unpayment);
            //     if (isset($createSecret['after']['message'])) {
            //         $checkCreateSecret = 'false';
            //         $secretId = 0;
            //     } else {
            //         $checkCreateSecret = 'true';
            //         $secretId = $createSecret['after']['ret'];
            //     }
            // } catch (\Exception $e) {
            //     $checkCreateSecret = 'false';
            //     $secretId = 0;
            // }
    
            $request->request->remove('password_confirmation');
            $request->request->add(['slug' => str($newNumber . '_' . $first_name)->slug()]);
            $request->request->add(['username' => $username]);
            $request->request->add(['password_ppp' => $passwordPpp]);
            $request->request->add(['password' => Hash::make($request->password)]);
            // $request->request->add(['secret_id' => $secretId]);
            // $request->request->add(['discount' => $discount]);
            $request->request->add(['customer_number' => str($newNumber)]);
            $request->request->add(['mikrotik_id' => $mikrotikId]);
    
    
            $customer = Customer::create($request->all());
    
            $getCustomer = $customer->fresh();
            CustomerOnt::create([
                'customer_id' => $getCustomer->id,
            ]);
    
            // $billing = Billing::create([
            //     'slug' => str($getCustomer->id . '-' . Str::random(4))->slug(),
            //     'month' => $month,
            //     'year' => $year,
            //     'billing_periode_id' => $periodeId,
            //     'billing_number' => str($getCustomer->id . '-' . Str::random(4)  . '-' . $month . '-' . $year)->slug(),
            //     'customer_id' => $getCustomer->id,
            //     'customer_name' => $getCustomer->name,
            //     'paket_name' => $getCustomer->paket->name,
            //     'paket_price' => $getCustomer->paket->price * ((100 - $getCustomer->discount) / 100),
            //     'created_name' => $getCustomer->name,
            //     'status' => 'BL'
            // ]);
        return redirect(route('customerLogin'))->withSuccess('Sedang di proses sialahkan tunggu konfirmasi dari admin');  
        }
        // return dd($customer);
        // return redirect(route('customer.register'))->withSuccess('Sialahkan tunggu konfirmasi dari admin');
    // }

    public function customerLogout(Request $request)
    {
        auth()->guard('customer')->logout();
        Session::flush();
        // Session::put('success', 'You are logout sucessfully');
        return redirect(route('customerLogin'))->withSuccess('You are logout sucessfully');
    }
}
