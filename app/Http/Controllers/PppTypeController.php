<?php

namespace App\Http\Controllers;

use App\Models\PppType;
use Illuminate\Http\Request;

class PppTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(PppType $pppType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PppType $pppType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, PppType $pppType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(PppType $pppType)
    {
        //
    }
}
