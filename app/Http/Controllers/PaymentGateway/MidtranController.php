<?php

namespace App\Http\Controllers\PaymentGateway;

use App\Models\Midtran;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MidtranController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Midtran $midtran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Midtran $midtran)
    {
        return view('payment_gateways.midtrans.edit', compact('midtran'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Midtran $midtran)
    {
        $this->validate($request, [
            'name' => ['required'],
            'midtrans_merchant_id' => 'required',
            'midtrans_client_key' => 'required',
            'midtrans_server_key' => 'required',
        ]);
        if ($request->midtrans_is_production == NULL) {
            $midtrans_is_production = 'false';
        } else {
            $midtrans_is_production = 'true';
        }

        if ($request->disabled == NULL) {
            $disabled = 'false';
        } else {
            $disabled = 'true';
        }
        $midtran->update([
            'name' => $request->name,
            'midtrans_merchant_id' => $request->midtrans_merchant_id,
            'midtrans_client_key' => $request->midtrans_client_key,
            'midtrans_server_key' => $request->midtrans_server_key,
            'midtrans_sandbox_client_key' => $request->midtrans_sandbox_client_key,
            'midtrans_sandbox_server_key' => $request->midtrans_sandbox_server_key,
            'midtrans_is_production' => $midtrans_is_production,
            'disabled' => $disabled,

        ]);

        return redirect()->back()->with('success', 'Configuration midtrans successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Midtran $midtran)
    {
        //
    }
}
