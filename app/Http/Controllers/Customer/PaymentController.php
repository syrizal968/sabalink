<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Models\Billing\Billing;
use App\Http\Controllers\Controller;
use App\Models\Websystem;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $customerId = (auth()->guard('customer')->user()->id);

        $state = request('status');
        $billings = Billing::with('orders')->where('customer_id', $customerId)
            ->when($state, function ($query) use ($state) {
                $query->where('status', $state);
            })
            ->orderBy('year', 'DESC')
            ->orderBy('month', 'DESC')
            ->orderBy('customer_name', 'ASC')->paginate(10);
            // dd($billings);
        return view('customers.pages.payments.index', compact('billings'))
            ->with('i', (request()->input('page', 1) - 1) * 10);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Billing $billing)
    {
        $paymentGateway = Websystem::where('slug', 'websystem')->value('payment_gateway');
        return view('customers.pages.payments.show', compact('billing', 'paymentGateway'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
