<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Paket;
use App\Models\Customer;
use App\Models\Mikrotik;
use Illuminate\Http\Request;
//use Yajra\DataTables\Facades\DataTables;
use App\Models\Billing\Billing;
use Spatie\Permission\Models\Role;
use App\Http\Resources\Pppoe\SecretResource;
use App\Services\Routerboard\Ppp\SecretService;
use App\Services\Routerboard\TestConnectionService;

class HomeController extends Controller
{
    private TestConnectionService $testConnectionService;
    private SecretService $secretService;

    public function __construct(SecretService $secretService, TestConnectionService $testConnectionService)
    {
        $this->testConnectionService = $testConnectionService;
        $this->secretService = $secretService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * Handle the incoming request.
     */

    public function __invoke(Request $request)
    {
        $mikrotiks = Mikrotik::orderBy('name', 'ASC')->get();
        $routerOnline = 0;
        $routerOffline = 0;
        foreach ($mikrotiks as $mikrotik) {
            $router = Mikrotik::where('id', $mikrotik->id)->first();
            try {
                $this->testConnectionService->checkOnline($router);
                $routerOnline++;
            } catch (\Exception $e) {
                $routerOffline++;
            }
        }
        $routersEnable = Mikrotik::where('disabled', 'false')->get();
        $customersOnline = 0;
        $customersOffline = 0;
        $sumCustomersOnline = 0;
        if (count($routersEnable) != 0) {

            foreach ($routersEnable as $routerEnable) {
                try {
                    $this->testConnectionService->checkOnline($routerEnable);
                    $usernameCustomers = Customer::where('mikrotik_id', $routerEnable->id)->pluck('username')->toArray();
                    $collectCustomerUsernames = collect($usernameCustomers);
                    $getAllSecretActive = $this->secretService->getPppActive($routerEnable);
                    $collectionSecretActive = SecretResource::collection($getAllSecretActive);
                    $customersOnline = $collectionSecretActive->count();
                    $secretActive = $collectionSecretActive->pluck(['name'])->toArray();
                    $secretsNonActive = $collectCustomerUsernames->diff($secretActive);

                    foreach ($secretsNonActive as $secretNonActive) {
                        $customersOffline++;
                    }
                } catch (Exception $e) {
                }
                $sumCustomersOnline += $customersOnline;
            }
        }

        $customers = Customer::all();
        $payments = Billing::where('status', 'LS')->get();
        $unpayments = Billing::where('status', 'BL')->get();
        $paylaters = Billing::where('status', 'PL')->get();
        return view('home', compact(
            'customersOffline',
            'sumCustomersOnline',
            'mikrotiks',
            'customers',
            'payments',
            'unpayments',
            'paylaters',
            'routerOnline',
            'routerOffline'
        ));
    }
}
