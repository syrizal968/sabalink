<?php

namespace App\Http\Controllers;

use App\Models\Paket;
use App\Models\Customer;
use App\Models\Mikrotik;
use Carbon\CarbonPeriod;
use App\Models\Websystem;
use App\Models\AutoIsolir;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Jobs\CreateBillingJob;
use Illuminate\Support\Carbon;
use App\Jobs\TestConnectionJob;
use App\Models\Billing\Billing;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use App\Models\Billing\BillingPeriode;
use App\Jobs\AddPaymentAfterDueDateJob;
use App\Jobs\AddPaymentBeforeDueDateJob;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\BillingStoreRequest;
use App\Jobs\CancelPaymentAfterDueDateJob;
use App\Jobs\CancelPaymentBeforeDueDateJob;
use App\Http\Resources\Pppoe\SecretResource;
use App\Http\Requests\BillingStoreOneRequest;
use App\Services\Routerboard\Ppp\SecretService;
use App\Services\Routerboard\TestConnectionService;
use PDF;
use Illuminate\Contracts\Database\Eloquent\Builder;


class BillingController extends Controller
{

    private SecretService $secretService;
    private TestConnectionService $testConnectionService;
    public function __construct(SecretService $secretService, TestConnectionService $testConnectionService)
    {
        $this->secretService = $secretService;
        $this->testConnectionService = $testConnectionService;
        $this->middleware('permission:billing-list|billing-create|billing-edit|billing-delete', ['only' => ['index', 'show', 'searchUnpayment', 'searchPayment']]);
        $this->middleware('permission:billing-create', ['only' => ['create', 'store', 'create_one', 'store_one']]);
        $this->middleware('permission:billing-edit', ['only' => ['edit', 'update', 'payment', 'unpayment']]);
        $this->middleware('permission:billing-delete', ['only' => ['destroy', 'destroyPeriode']]);
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $print = $request->cetak_pdf;

        $searchName = $request->search_name;
        $searchMonth = $request->search_month;
        $searchYear = $request->search_year;
        $searchStatus = $request->search_status;
        $searchAddress = $request->search_address;

        




        $billings = Billing::query()
            ->when($searchName, function (Builder $query) use ($searchName) {
                $query->where('customer_name', 'like', "%{$searchName}%");
            })
            ->when($searchMonth, function (Builder $query) use ($searchMonth) {
                $query->where('month', $searchMonth);
            })
            ->when($searchYear, function (Builder $query) use ($searchYear) {
                $query->where('year', $searchYear);
            })
            ->when($searchStatus, function (Builder $query) use ($searchStatus) {
                $query->where('status', $searchStatus);
            })
            ->orderBy('year', 'DESC')
            ->orderBy('month', 'DESC')
            ->orderBy(
                Customer::select('address')
                    ->whereColumn('customers.id', 'billings.customer_id')
            )
            ->orderBy('customer_name', 'ASC');

        if ($searchAddress) {
            if ($searchAddress == 'NULL') {
                $addressCustomer = Customer::select('id')->where('address',  NULL)->get();
            } else {
                $addressCustomer = Customer::select('id')->where('address',  $searchAddress)->get();
            }
            $billingInAddress = $billings->whereIn('customer_id', $addressCustomer);
            $dataPrint = $billingInAddress->get();
            $totalPrice = $billingInAddress->sum('paket_price');
            $data = $billingInAddress->paginate(15);
        } else {
            $dataPrint = $billings->get();
            $totalPrice = $billings->sum('paket_price');
            $data = $billings->paginate(15);
        }

        if ($print == 'true') {
            $month = 'Semua Bulan';
            if ($searchMonth) {
                $dateObj   = Carbon::createFromFormat('!m', $searchMonth);
                $month = $dateObj->format('F');
            }
            if ($searchStatus == 'LS') {
                $status = 'Lunas';
            } else if ($searchStatus == 'BL') {
                $status = 'Belum Lunas';
            } else if ($searchStatus == 'PL') {
                $status = 'Mundur';
            } else {
                $status = 'Semua Status';
            }

            if ($searchAddress) {
                $address = $searchAddress;
            } else {
                $address = 'Semua alamat';
            }

            if ($searchName) {
                $name = $searchAddress;
            } else {
                $nama = 'Semua Customer';
            }

            $search = [
                'month' => $month,
                'year' => $searchYear,
                'status' => $status,
                'name' => $nama,
                'address' => $address

            ];
            $pdf = PDF::loadView('print.billing', ['dataPrint' => $dataPrint], $search);
            return $pdf->download('Tagihan_' . str($nama)->slug() . '_' . str($address)->slug() . '_' . str($status)->slug() . '_' . str($month)->slug() . '_' . $searchYear . '.pdf');
        } else {
            if (count($data) == 0) {
                return view('billings.index', compact('data', 'totalPrice'), [
                    'message' => 'No billing found. Try to search again !',
                ]);
            } else {
                return view('billings.index', compact('data', 'totalPrice'))
                    ->with('i', (request()->input('page', 1) - 1) * 25);
            }
        }
    }


    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $billingPeriodeName = BillingPeriode::latest('created_at')->value('name');
        $periodeNow = Carbon::now()->format('F Y');
        if ($periodeNow == $billingPeriodeName) {
            $billingPeriodes = CarbonPeriod::create(now()->addmonth(), '1 month', now()->addMonths(12));
        } else {
            $billingPeriodes = CarbonPeriod::create(now(), '1 month', now()->addMonths(11));
        }
        $data = BillingPeriode::paginate(12);

        return view('billings.create', compact('data', 'billingPeriodes'))
            ->with('i', (request()->input('page', 1) - 1) * 12);
    }
    public function create_one()
    {
        $periodes = BillingPeriode::latest()->get();
        return view('billings.create_one', compact('periodes'));
    }
    /**
     * Store a newly created resource in storage.
     * Add Comment "Belum Lunas" on User Secret Mikrotik 
     */
    // public function store(BillingStoreRequest $request)
    public function store(Request $request)
    {
        $periode = $request->name;
        if (!$periode) {
            $periode = Carbon::now()->format('F Y');
            $request->request->add(['name' => $periode]);
        }
        $this->validate(
            $request,
            [
                'name' => 'unique:billing_periodes',
            ],
            [
                'name.unique' => 'Periode ' . $request->name . ' already exist.',
            ]
        );
        $billingPeriode = BillingPeriode::create([
            'name' => $periode,
        ]);

        $totalBillingCreated = 0;
        $routers = Mikrotik::all();
        foreach ($routers as $router) {
            $customers = Customer::where('disabled', 'false')
                ->where('activation', 'true')
                ->where('mikrotik_id', $router->id)
                ->get();
            $monthYear = date_parse($periode);
            $month = $monthYear['month'];
            $year = $monthYear['year'];
            $billing = Billing::select('customer_id')->where('month', $month)
                ->where('year', $year)
                ->get();

            $customersUnbilling = $customers->diff(
                Customer::whereIn('id', $billing)
                    ->get()
            );

            foreach ($customersUnbilling as $customer) {
                $customerPriceAfterDiscount = $customer->paket->price * ((100 - $customer->discount) / 100);
                $customerActivationDate = Carbon::parse($customer->activation_date)->format('d');
                $periodeName = BillingPeriode::latest('created_at')->value('name');
                $nextMonth = Carbon::parse($periodeName)->addMonth()->format('m_Y');
                if ($customerActivationDate >= 29) {
                    $customerActivationDate = 28;
                }
                $secretId = $customer->secret_id;
                $mikrotikId = $customer->mikrotik_id;
                $router = Mikrotik::where('id', $mikrotikId)->first();
                $autoIsolir = AutoIsolir::where('mikrotik_id', $mikrotikId)->first();
                if ($router->disabled == 'false') {
                    if ($autoIsolir != NULL) {
                        if ($autoIsolir->disabled == 'false') {
                            if ($autoIsolir->activation_date == 'false') {
                                if ($customerPriceAfterDiscount > 0) {
                                    $comment = $autoIsolir->comment_unpayment;
                                } else {
                                    $comment = $autoIsolir->comment_payment;
                                }

                                try {
                                    dispatch(new CreateBillingJob($router, $comment, $secretId))->onQueue('default');
                                } catch (\Exception $e) {
                                    $billingPeriode->delete();
                                    return redirect()->back()->withErrors(['msg' => 'Server ' . $router->name . ' offline, please check your router.']);
                                }
                            } else {
                                if ($customerPriceAfterDiscount == 0.0) {
                                    $comment = $autoIsolir->comment_unpayment . '_' . $customerActivationDate . '_' . $nextMonth;
                                    try {
                                        dispatch(new CreateBillingJob($router, $comment, $secretId))->onQueue('default');
                                    } catch (\Exception $e) {
                                        $billingPeriode->delete();
                                        return redirect()->back()->withErrors(['msg' => 'Server ' . $router->name . ' offline, please check your router.']);
                                    }
                                }
                            }
                        }
                    }
                }

                if ($customerPriceAfterDiscount == 0.0) {
                    $status = 'LS';
                } else {
                    $status = 'BL';
                }

                Billing::create([
                    'billing_periode_id' => $billingPeriode->id,
                    'slug' => str($customer->id . '-' . Str::random(4))->slug(),
                    'month' => $month,
                    'year' => $year,
                    'billing_number' => str($customer->id . '-' . Str::random(4)  . '-' . $month . '-' . $year)->slug(),
                    'customer_id' => $customer->id,
                    'customer_name' => $customer->name,
                    'paket_name' => $customer->paket->name,
                    'paket_price' => $customer->paket->price * ((100 - $customer->discount) / 100),
                    'created_name' => auth()->user()->name,
                    'status' => $status,
                ]);
                $totalBillingCreated++;
            }
        }

        return redirect()->route('billings.index')
            ->with('success', $totalBillingCreated . ' Billing created successfully.');
    }

    public function store_one(BillingStoreRequest $request)
    {
        $billingPeriodeId = $request->name;
        $periodeName = BillingPeriode::where('id', $billingPeriodeId)->value('name');
        $month = Carbon::parse($periodeName)->format('m');
        $year = Carbon::parse($periodeName)->format('Y');

        $totalBillingCreated = 0;
        $routers = Mikrotik::all();
        foreach ($routers as $router) {
            $customers = Customer::where('disabled', 'false')->where('mikrotik_id', $router->id)->where('activation', 'true')->get();
            $billing = Billing::select('customer_id')->where('month', $month)
                ->where('year', $year)
                ->get();

            $customersUnbilling = $customers->diff(
                Customer::whereIn('id', $billing)
                    ->get()
            );

            foreach ($customersUnbilling as $customer) {
                $customerPriceAfterDiscount = $customer->paket->price * ((100 - $customer->discount) / 100);
                $customerActivationDate = Carbon::parse($customer->activation_date)->format('d');
                $periodeName = BillingPeriode::latest('created_at')->value('name');
                $nextMonth = Carbon::parse($periodeName)->addMonth()->format('m_Y');
                if ($customerActivationDate >= 29) {
                    $customerActivationDate = 28;
                }
                $secretId = $customer->secret_id;
                $mikrotikId = $customer->mikrotik_id;
                $router = Mikrotik::where('id', $mikrotikId)->first();
                $autoIsolir = AutoIsolir::where('mikrotik_id', $mikrotikId)->first();
                if ($router->disabled == 'false') {
                    if ($autoIsolir != NULL) {
                        if ($autoIsolir->disabled == 'false') {
                            if ($autoIsolir->activation_date == 'false') {
                                if ($customerPriceAfterDiscount > 0) {
                                    $comment = $autoIsolir->comment_unpayment;
                                } else {
                                    $comment = $autoIsolir->comment_payment;
                                }
                                try {
                                    dispatch(new CreateBillingJob($router, $comment, $secretId))->onQueue('default');
                                } catch (\Exception $e) {
                                    return redirect()->back()->withErrors(['msg' => 'Server ' . $router->name . ' offline, please check your router.']);
                                }
                            } else {
                                if ($customerPriceAfterDiscount == 0.0) {
                                    $comment = $autoIsolir->comment_unpayment . '_' . $customerActivationDate . '_' . $nextMonth;
                                    try {
                                        dispatch(new CreateBillingJob($router, $comment, $secretId))->onQueue('default');
                                    } catch (\Exception $e) {
                                        return redirect()->back()->withErrors(['msg' => 'Server ' . $router->name . ' offline, please check your router.']);
                                    }
                                }
                            }
                        }
                    }
                }

                if ($customerPriceAfterDiscount == 0.0) {
                    $status = 'LS';
                } else {
                    $status = 'BL';
                }
                Billing::create([
                    'billing_periode_id' => $billingPeriodeId,
                    'slug' => str($customer->id . '-' . Str::random(4))->slug(),
                    'month' => $month,
                    'year' => $year,
                    'billing_number' => str($customer->id . '-' . Str::random(4)  . '-' . $month . '-' . $year)->slug(),
                    'customer_id' => $customer->id,
                    'customer_name' => $customer->name,
                    'paket_name' => $customer->paket->name,
                    'paket_price' => $customer->paket->price * ((100 - $customer->discount) / 100),
                    'created_name' => auth()->user()->name,
                    'status' => $status
                ]);
                $totalBillingCreated++;
            }
        }
        return redirect()->route('billings.index')
            ->with('success', $totalBillingCreated . ' Billing created successfully.');
    }

    /**
     * Display the specified resource.
     */
    public function show(Billing $billing)
    {
        $billingCustomerId = $billing->customer_id;
        $billings = Billing::where('customer_id', $billingCustomerId)
            ->where('status', '!=', 'LS')
            ->get();
        $totalPrice = Billing::where('customer_id', $billingCustomerId)
            ->where('status', '!=', 'LS')
            ->sum('paket_price');
        return view('billings.show', compact('billing', 'totalPrice'), [
            'billings' => $billings,
        ]);
    }
    public function paylater(Request $request, Billing $billing)
    {
        $billingCustomerId = $billing->customer_id;
        $billings = Billing::where('customer_id', $billingCustomerId)->where('status', 'BL')->get();
        return view('billings.paylater', compact('billing'), [
            'billings' => $billings,
        ]);
    }

    public function post_paylater(Request $request, Billing $billing)
    {
        $this->validate($request, [
            'pay_later' => 'required',
        ]);
        $day = Carbon::now()->format('d');
        $paylaterDate = Carbon::parse($request->pay_later)->format('d');
        $paylaterMonth = Carbon::parse($request->pay_later)->format('m_Y');
        $customer = Customer::where('id', $billing->customer_id)->first();
        $customerPaketId = $customer->paket_id;
        $customerProfile = Paket::where('id', $customerPaketId)->value('profile');
        $customerSecretId = $customer->secret_id;
        $customerUsername = $customer->username;
        $mikrotikId = $customer->mikrotik_id;
        $router = Mikrotik::where('id', $mikrotikId)->first();
        $autoIsolir = AutoIsolir::where('mikrotik_id', $mikrotikId)->first();
        if ($router->disabled == 'false' && $autoIsolir != NULL && $autoIsolir->disabled == 'false') {
            try {
                dispatch(new TestConnectionJob($router))->onQueue('default');
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(['msg' => 'Server ' . $router->name . ' offline, please check your router.']);
            }
            if ($autoIsolir->activation_date == 'false') {
                $comment_payment = $autoIsolir->comment_payment;
                $dueDate = $autoIsolir->due_date;
            } else {
                $dueDate = $paylaterDate;
                if ($paylaterDate >= 29) {
                    $paylaterDate = 28;
                }
                $comment_payment = $autoIsolir->comment_unpayment . '_' . $paylaterDate . '_' . $paylaterMonth;
            }

            $userSecretId = $this->secretService->getSecretById($router, $customerSecretId);
            if ($userSecretId != 'error') {
                if ($day <= $dueDate) {
                    $secretProfiles = SecretResource::collection($userSecretId)->value('profile');
                    if ($secretProfiles == $customerProfile) {
                        dispatch(new AddPaymentBeforeDueDateJob($router, $comment_payment, $customerSecretId))->onQueue('default');
                    } else {
                        dispatch(new AddPaymentAfterDueDateJob($router, $customerProfile, $comment_payment, $customerSecretId, $customerUsername))->onQueue('default');
                    }
                } else {
                    dispatch(new AddPaymentAfterDueDateJob($router, $customerProfile, $comment_payment, $customerSecretId, $customerUsername))->onQueue('default');
                }
            } else {
                return redirect()->back()->withErrors(['msg' => $customerSecretId . '! Canot find user "' . $customer->username . '" on server ' . $router->name . '.']);
            }
            // }
        }


        $billing->update([
            'pay_later' => $request->pay_later,
            'teller_name' => auth()->user()->name,
            'status' => 'PL'
        ]);
        return redirect()->back()->with('success', 'Payment successfully.');
    }

    public function payment(Request $request, Billing $billing)
    {
        $day = Carbon::now()->format('d');
        $customer = Customer::where('id', $billing->customer_id)->first();
        $customerPaketId = $customer->paket_id;
        $customerSecretId = $customer->secret_id;
        $customerUsername = $customer->username;
        $activationDate = Carbon::parse($customer->activation_date)->format('d');
        $periodeName = BillingPeriode::latest('created_at')->value('name');
        $nextMonth = Carbon::parse($periodeName)->addMonth()->format('m_Y');
        $mikrotikId = $customer->mikrotik_id;
        $customerProfile = Paket::where('id', $customerPaketId)->value('profile');
        $router = Mikrotik::where('id', $mikrotikId)->first();
        $autoIsolir = AutoIsolir::where('mikrotik_id', $mikrotikId)->first();

        if ($router->disabled == 'false' && $autoIsolir != NULL && $autoIsolir->disabled == 'false') {
            try {
                dispatch(new TestConnectionJob($router))->onQueue('default');
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(['msg' => 'Server ' . $router->name . ' offline, please check your server.']);
            }
            if ($autoIsolir->activation_date == 'false') {
                $comment_payment = $autoIsolir->comment_payment;
                $dueDate = $autoIsolir->due_date;
            } else {
                if ($activationDate >= 29) {
                    $activationDate = 28;
                }
                $comment_payment = $autoIsolir->comment_unpayment . '_' . $activationDate . '_' . $nextMonth;
                $dueDate = $activationDate;
            }

            $userSecretId = $this->secretService->getSecretById($router, $customerSecretId);
            if ($userSecretId != 'error') {
                if ($day <= $dueDate) {
                    $secretProfiles = SecretResource::collection($userSecretId)->value('profile');
                    if ($secretProfiles == $customerProfile) {
                        dispatch(new AddPaymentBeforeDueDateJob($router, $comment_payment, $customerSecretId))->onQueue('default');
                    } else {
                        dispatch(new AddPaymentAfterDueDateJob($router, $customerProfile, $comment_payment, $customerSecretId, $customerUsername))->onQueue('default');
                    }
                } else {
                    dispatch(new AddPaymentAfterDueDateJob($router, $customerProfile, $comment_payment, $customerSecretId, $customerUsername))->onQueue('default');
                }
            } else {
                return redirect()->back()->withErrors(['msg' => $customerSecretId . '! Canot find user "' . $customer->username . '" on server ' . $router->name . '.']);
            }
            // }
        }

        $billing->update([
            'payment_time' => Carbon::now()->format('Y-m-d h:i:s'),
            'teller_name' => auth()->user()->name,
            'status' => 'LS'
        ]);
        return redirect()->back()->with('success', 'Payment successfully.');
    }

    public function unpayment(Billing $billing)
    {
        $day = Carbon::now()->format('d');
        $customer = Customer::where('id', $billing->customer_id)->first();
        $secretId = $customer->secret_id;
        $username = $customer->username;
        $mikrotikId = $customer->mikrotik_id;
        $customerDueDate = $customer->activation_date;
        $date = Carbon::parse($customerDueDate)->format('d');
        $periodeName = BillingPeriode::latest('created_at')->value('name');
        $month = Carbon::parse($periodeName)->format('m');
        $year = Carbon::parse($periodeName)->format('Y');
        $autoIsolir = AutoIsolir::where('mikrotik_id', $mikrotikId)->first();
        if ($autoIsolir != NULL) {
            $profileIsolir = $autoIsolir->profile_id;
            if ($autoIsolir->activation_date == 'false') {
                $comment_unpayment = $autoIsolir->comment_unpayment;
                $dueDate = $autoIsolir->due_date;
            } else {
                $comment_unpayment = $autoIsolir->comment_unpayment . '_' . $date . '_' . $month . '_' . $year;
                $dueDate = $date;
            }
            $router = Mikrotik::where('id', $mikrotikId)->first();
            try {
                dispatch(new TestConnectionJob($router))->onQueue('default');
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(['msg' => 'Router offline, please check your router.']);
            }
            if ($day <= $dueDate) {
                dispatch(new CancelPaymentBeforeDueDateJob($router, $comment_unpayment, $secretId))->onQueue('default');
            } else {
                dispatch(new CancelPaymentAfterDueDateJob($router, $comment_unpayment, $profileIsolir, $secretId, $username))->onQueue('default');
            }
        }
        $billing->update([
            'teller_name' => auth()->user()->name,
            'status' => 'BL'
        ]);
        return redirect()->back()->with('success', 'Cancel payment successfully.');
    }

    public function deletePeriode(BillingPeriode $billingperiode)
    {
        return view('billings.delete', compact('billingperiode'));
    }

    public function destroyPeriode(BillingPeriode $billingperiode)
    {
        $billingperiode->delete();
        return redirect()->route('billings.create')->with('success', 'Periode billing deleted successfully.');
    }
}
