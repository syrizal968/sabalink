<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Order;
use App\Models\Paket;
use App\Models\PppType;
use App\Models\Customer;
use App\Models\Mikrotik;
use App\Models\Websystem;
use Illuminate\View\View;
use App\Models\AutoIsolir;
use App\Models\MerkRouter;
use App\Models\TypeRouter;
use App\Models\CustomerOnt;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Billing\Billing;
use App\Models\DeletedCustomer;
use Illuminate\Validation\Rule;
use App\Jobs\DeletePppSecretJob;
use App\Jobs\EnablePppSecretJob;
use App\Jobs\DisablePppSecretJob;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\PaketResource;
use Illuminate\Http\RedirectResponse;
use App\Models\Billing\BillingPeriode;
use App\Jobs\AddPaymentAfterDueDateJob;
use RouterOS\Exceptions\QueryException;
use App\Jobs\AddPaymentBeforeDueDateJob;
use RouterOS\Exceptions\ClientException;
use RouterOS\Exceptions\ConfigException;
use RouterOS\Exceptions\ConnectException;
use App\Http\Requests\CustomerStoreRequest;
use App\Http\Requests\CustomerUpdateRequest;
use App\Http\Resources\Pppoe\SecretResource;
use App\Http\Requests\CustomerPppUpdateRequest;
use App\Services\Routerboard\Ppp\SecretService;
use RouterOS\Exceptions\BadCredentialsException;
use App\Http\Requests\CustomerRouterUpdateRequest;
use App\Services\Routerboard\TestConnectionService;
use Illuminate\Contracts\Database\Eloquent\Builder;

class CustomerController extends Controller
{
    /**
     * @throws ClientException
     * @throws ConnectException
     * @throws QueryException
     * @throws BadCredentialsException
     * @throws ConfigException
     */
    private SecretService $secretService;
    private TestConnectionService $testConnectionService;



    public function __construct(SecretService $secretService, TestConnectionService $testConnectionService)
    {
        $this->secretService = $secretService;
        $this->testConnectionService = $testConnectionService;
        $this->middleware('permission:customer-list|customer-create|customer-edit|customer-delete|secret-disable|secret-edit', ['only' => ['index', 'show', 'showCustomerOffline']]);
        $this->middleware('permission:customer-create', ['only' => ['create', 'store', 'setting', 'exportToRouter']]);
        $this->middleware('permission:customer-edit', ['only' => ['edit', 'update', 'setting', 'exportToRouter', 'editRouter', 'updateRouter', 'editPpp', 'updatePpp']]);
        $this->middleware('permission:customer-delete', ['only' => ['destroy', 'delete']]);
        $this->middleware('permission:secret-disable', ['only' => ['disable', 'enable']]);
        $this->middleware('permission:secret-edit', ['only' => ['editPpp', 'updatePpp']]);
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): View
    {
        $searchName = $request->search_name;
        $searchAddress = $request->search_address;
        $searchServer = $request->search_server;
        $searchPaket = $request->search_paket;
        $searchStatus = $request->disabled;

        $customers = Customer::query()
            ->when($searchName, function (Builder $query) use ($searchName) {
                $query->where('name', 'like', "%{$searchName}%");
            })
            ->when($searchAddress, function (Builder $query) use ($searchAddress) {
                if ($searchAddress == 'NULL') {
                    $query->where('address', NULL);
                } else {
                    $query->where('address', $searchAddress);
                }
            })
            ->when($searchStatus, function (Builder $query) use ($searchStatus) {
                $query->where('disabled', $searchStatus);
            })
            ->when($searchServer, function (Builder $query) use ($searchServer) {
                $query->where('mikrotik_id', $searchServer);
            })
            ->when($searchPaket, function (Builder $query) use ($searchPaket) {
                $query->where('paket_id', $searchPaket);
            })
            ->orderBy('address', 'ASC')
            ->orderBy('name', 'ASC');

        $customersGet = $customers->get();
        $data = $customers->paginate(20);

        $totalIncoming = 0;
        foreach ($customersGet as $customerGet) {
            $totalIncoming += $customerGet->paket->price * ((100 - $customerGet->discount) / 100);
        }

        if (count($data) == 0) {
            return view('customers.index', compact('data', 'totalIncoming'), [
                'message' => 'No customer found. Try to search again !',
            ]);
        } else {
            return view('customers.index', compact('data', 'totalIncoming'))
                ->with('i', (request()->input('page', 1) - 1) * 20);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {

        $mikrotiks = Mikrotik::where('disabled', 'false')->orderBy('name', 'ASC')->pluck('name', 'id');
        $pppservices = PppType::orderBy('name', 'ASC')->pluck('name', 'id');

        return view('customers.create', [
            'mikrotiks' => $mikrotiks,
            'pppservices' => $pppservices,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CustomerStoreRequest $request): RedirectResponse
    {
		if ($request->discount) {
            $discount = $request->discount;
        } else {
            $discount = 0;
        }

        $date = Carbon::now()->format('d');
        if ($date >= 29) {
            $date = 28;
        }
        $periodeId = BillingPeriode::latest('created_at')->value('id');
        if (!isset($periodeId)) {
            $billingPeriode = BillingPeriode::create([
                'name' => Carbon::now()->format('F Y'),
            ]);
            $newBillingPeriode = $billingPeriode->fresh();
            $periodeId = $newBillingPeriode->id;
            $periodeName = $newBillingPeriode->name;
        } else {
            $periodeName = BillingPeriode::latest('created_at')->value('name');
        }
        $month = Carbon::parse($periodeName)->format('m');
        $year = Carbon::parse($periodeName)->format('Y');

        $name = explode(' ', $request->name);
        $first_name = $name[0];
        //Informasi ppp
         // ambil data pelanggan terakhir
        //  $lastCustomer = Customer::latest()->first();
        //  if ($lastCustomer->customer_number !== null) {
           // buat nomor pelanggan
        //      $newNumber = $lastCustomer->id + 1 . Carbon::now()->format('Ymd');
        //  } else {
        //      $newNumber = $lastCustomer->id + 1 . Carbon::now()->format('Ymd');
        //  }
        $lastCustomer = Customer::latest()->first();
        if ($lastCustomer) {
            $newNumber = ($lastCustomer->id + 1) . Carbon::now()->format('Ymd');
        } else {
            $newNumber = '1' . Carbon::now()->format('Ymd');
        }
        $username = $newNumber . '_' . strtoupper($first_name);
        $passwordPpp = '12345';

        $mikrotikId = $request->mikrotik_id;
        $router = Mikrotik::where('id', $mikrotikId)->first();

        $autoIsolir = AutoIsolir::where('mikrotik_id', $mikrotikId)->first();
        if (isset($autoIsolir)) {
            if ($autoIsolir->activation_date == 'true') {
                $comment_unpayment = $autoIsolir->comment_unpayment . '_' . $date . '_' . $month . '_' . $year;
            } else {
                $comment_unpayment = $autoIsolir->comment_unpayment;
            }
            $firstProfile = $autoIsolir->profile_id;
        } else {
            $firstProfile = Paket::where('mikrotik_id', $mikrotikId)->where('id', $request->paket_id)->value('profile');
            $comment_unpayment = 'create_from_customer_management';
        }
        $serviceName = PppType::where('id', $request->ppp_type_id)->value('name');

        try {

            $createSecret = $this->secretService->createSecret($router, $username, $passwordPpp, $serviceName, $firstProfile, $comment_unpayment);
            if (isset($createSecret['after']['message'])) {
                $checkCreateSecret = 'false';
                $secretId = 0;
            } else {
                $checkCreateSecret = 'true';
                $secretId = $createSecret['after']['ret'];
            }
        } catch (\Exception $e) {
            $checkCreateSecret = 'false';
            $secretId = 0;
        }

        $request->request->remove('password_confirmation');
        $request->request->add(['slug' => str($newNumber . '_' . $first_name)->slug()]);
        $request->request->add(['username' => $username]);
        $request->request->add(['password_ppp' => $passwordPpp]);
        $request->request->add(['password' => Hash::make($request->password)]);
        $request->request->add(['secret_id' => $secretId]);
		$request->request->add(['discount' => $discount]);
        $request->request->add(['customer_number' => $newNumber]);


        $customer = Customer::create($request->all());

        $getCustomer = $customer->fresh();
        CustomerOnt::create([
            'customer_id' => $getCustomer->id,
        ]);

        $billing = Billing::create([
            'slug' => str($getCustomer->id . '-' . Str::random(4))->slug(),
            'month' => $month,
            'year' => $year,
            'billing_periode_id' => $periodeId,
            'billing_number' => str($getCustomer->id . '-' . Str::random(4)  . '-' . $month . '-' . $year)->slug(),
            'customer_id' => $getCustomer->id,
            'customer_name' => $getCustomer->name,
            'paket_name' => $getCustomer->paket->name,
            'paket_price' => $getCustomer->paket->price * ((100 - $getCustomer->discount) / 100),
            'created_name' => auth()->user()->name,
            'status' => 'BL'
        ]);
        if ($checkCreateSecret == 'true') {
            return redirect()->route('customers.index')
                ->with('success', 'Customer added successfully.');
        } else {
            return redirect()->route('customers.index')
                ->with('success', 'Customer added successfully but cant create user secret on server ' . $router->name . '.');
        }
    }

    public function activation(Customer $customer): RedirectResponse
    {
        $activationDate = Carbon::now()->format('d F Y');

        $billing = Billing::where('customer_id', $customer->id)->first();
        $billing->update([
            'payment_time' => Carbon::now()->format('Y-m-d h:i:s'),
            'teller_name' => auth()->user()->name,
            'status' => 'LS'
        ]);

        $mikrotikId = $customer->mikrotik_id;
        $profile = Paket::where('mikrotik_id', $mikrotikId)->where('id', $customer->paket_id)->value('profile');
        $router = Mikrotik::where('id', $mikrotikId)->first();
        if ($router->disabled == 'true') {
            return redirect()->back()->withErrors(['msg' => 'Server ' . $router->name . ' disable, please enable first.']);
        }

        $autoIsolir = AutoIsolir::where('mikrotik_id', $mikrotikId)->first();
        if (isset($autoIsolir)) {
            if ($autoIsolir->activation_date == 'true') {
                $day = Carbon::now()->format('d');
                if ($day >= 29) {
                    $day = 28;
                }
                $nextMonth = Carbon::now()->addMonth()->format('m_Y');
                $comment_payment = $autoIsolir->comment_unpayment . '_' . $day . '_' . $nextMonth;
            } else {
                $comment_payment = $autoIsolir->comment_payment;
            }
        } else {
            $comment_payment = 'create_from_customer_management';
        }

        $username = $customer->username;

        if ($customer->secret_id == 0) {
            //Password ppp
            $password = $customer->password_ppp;
            $serviceName = PppType::where('id', $customer->ppp_type_id)->value('name');

            try {
                $createSecret = $this->secretService->createSecret($router, $username, $password, $serviceName, $profile, $comment_payment);
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(['msg' => 'Server ' . $router->name . ' offline, please check your router.']);
            }


            if (isset($createSecret['after']['message'])) {
                return redirect()->back()->withErrors(['msg' => $createSecret['after']['message'], 'Maybe, profile ' . $profile . ' on your mikrotik not found.']);
            } else {
                $customer->update([
                    'activation_date' => $activationDate,
                    'activation' => 'true',
                    'disabled' => 'false',
                    'secret_id' => $createSecret['after']['ret'],
                ]);

                return redirect()->back()
                    ->with('success', 'User secret created on server ' . $router->name . ', Customer activated.');
            }
        } else {
            try {
                dispatch(new AddPaymentAfterDueDateJob($router, $profile, $comment_payment, $customer->secret_id, $username))->onQueue('default');
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(['msg' => 'Server ' . $router->name . ' offline, please check your router.']);
            }

            $customer->update([
                'activation_date' => $activationDate,
                'activation' => 'true',
                'disabled' => 'false',
            ]);
            return redirect()->back()
                ->with('success', 'Customer activated.');
        }
    }

    public function activations(Customer $customer): JsonResponse
    {
        $activationDate = Carbon::now()->format('d F Y');

        $billing = Billing::where('customer_id', $customer->id)->first();
        $billing->update([
            'payment_time' => Carbon::now()->format('Y-m-d h:i:s'),
            'teller_name' => auth()->guard('customer')->user()->name,
            'status' => 'LS'
        ]);

        $mikrotikId = $customer->mikrotik_id;
        $profile = Paket::where('mikrotik_id', $mikrotikId)->where('id', $customer->paket_id)->value('profile');
        $router = Mikrotik::where('id', $mikrotikId)->first();
        if ($router->disabled == 'true') {
            return redirect();
        }

        $autoIsolir = AutoIsolir::where('mikrotik_id', $mikrotikId)->first();
        if (isset($autoIsolir)) {
            if ($autoIsolir->activation_date == 'true') {
                $day = Carbon::now()->format('d');
                if ($day >= 29) {
                    $day = 28;
                }
                $nextMonth = Carbon::now()->addMonth()->format('m_Y');
                $comment_payment = $autoIsolir->comment_unpayment . '_' . $day . '_' . $nextMonth;
            } else {
                $comment_payment = $autoIsolir->comment_payment;
            }
        } else {
            $comment_payment = 'create_from_customer_management';
        }

        $username = $customer->username;

        if ($customer->secret_id == 0) {
            //Password ppp
            $password = $customer->password_ppp;
            $serviceName = PppType::where('id', $customer->ppp_type_id)->value('name');

            try {
                $createSecret = $this->secretService->createSecret($router, $username, $password, $serviceName, $profile, $comment_payment);
            } catch (\Exception $e) {
                return redirect();
            }


            if (isset($createSecret['after']['message'])) {
                return redirect();
            } else {
                $customer->update([
                    'activation_date' => $activationDate,
                    'activation' => 'true',
                    'disabled' => 'false',
                    'secret_id' => $createSecret['after']['ret'],
                ]);

                return redirect();
            }
        } else {
            try {
                dispatch(new AddPaymentAfterDueDateJob($router, $profile, $comment_payment, $customer->secret_id, $username))->onQueue('default');
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(['msg' => 'Server ' . $router->name . ' offline, please check your router.']);
            }

            $customer->update([
                'activation_date' => $activationDate,
                'activation' => 'true',
                'disabled' => 'false',
            ]);
            return redirect();
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Customer $customer)
    {
        $billingLastPayment = Billing::where('customer_id', $customer->id)
            ->where('status', 'LS')
            ->latest('created_at')
            ->first();

        $activation = $customer->activation_date;
        if ($billingLastPayment == NULL) {
            $billingLastUnpayment = Billing::where('customer_id', $customer->id)
                ->where('status', '!=', 'LS')
                ->latest('created_at')
                ->first();
            if ($billingLastUnpayment == NULL) {
                $nextPayment = Carbon::parse($activation)->format('d F Y');
            } else {
                $lastUnpayment = BillingPeriode::where('id', $billingLastUnpayment->billing_periode_id)->value('name');
                $nextPayment = Carbon::parse($activation)->format('d ') . Carbon::parse($lastUnpayment)->format('F Y');
            }
            $lastPayment = 'No Payment';
        } else {
            $lastPayment = Carbon::parse($billingLastPayment->updated_at)->format('d F Y ');
            $nextPayment = Carbon::parse($activation)->format('d ') . Carbon::parse($lastPayment)->addMonth()->format('F Y');
        }

        $unpaid = Billing::where('customer_id', $customer->id)->where('status', '!=', 'LS')->get();
        $notYetPaid = collect($unpaid)->sum('paket_price');
        $router = Mikrotik::where('id', $customer->mikrotik_id)->first();
        $ont = CustomerOnt::where('customer_id', $customer->id)->first();
        try {
            $customerSecret = $this->secretService->getSecretById($router, $customer->secret_id);
            if ($customerSecret == 'error') {
                return redirect()->route('customers.index')->withErrors(['msg' => 'Cannot find user with name ' . $customer->username . ' on router ' . $customer->mikrotik->name . '!']);
            } else {
                $secret = SecretResource::collection($customerSecret)->first();
                $customerSecretActive = $this->secretService->getOnePppSecretActive($router, $customer->username);
                $secretActive = SecretResource::collection($customerSecretActive)->first();
            }
            $serverDisconnect = 'false';
            $message = 'Server ' . $router->name . ' online';
        } catch (\Exception $e) {
            $secret = '';
            $secretActive = '';
            $serverDisconnect = 'true';
            $message = 'Server ' . $router->name . ' offline, data displayed is local data. You cannot change this information.';
        }
        return view('customers.show', [
            'customer' => $customer,
            'pppsecret' => $secret,
            'mikrotik' => $router,
            'secretActive' => $secretActive,
            'ont' => $ont,
            'lastPayment' => $lastPayment,
            'nextPayment' => $nextPayment,
            'notYetPaid' => $notYetPaid,
            'serverDisconnect' => $serverDisconnect,
            'info_msg' => $message,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Customer $customer)
    {
        $router = Mikrotik::where('id', $customer->mikrotik_id)->first();
        if ($router->disabled == 'false') {
            try {
                $this->testConnectionService->checkOnline($router);
                $serverDisconnect = 'false';
            } catch (\Exception $e) {
                $serverDisconnect = 'true';
            }
        } else {
            $serverDisconnect = 'true';
        }
        $pakets = Paket::where('mikrotik_id', $customer->mikrotik_id)->where('disabled', 'false')->orderBy('price', 'ASC')->get();
        return view('customers.edit', compact('customer', 'pakets', 'serverDisconnect'));
    }

    public function update(CustomerUpdateRequest $request, Customer $customer)
    {
        $discount = $request->discount;
        if ($request->password) {
            $customer->update(['password' => Hash::make($request->password)]);
        }
        $router = Mikrotik::where('id', $customer->mikrotik_id)->first();
        $autoIsolir = AutoIsolir::where('mikrotik_id', $customer->mikrotik_id)->first();
        if ($request->paket_id != $customer->paket_id) {

            if ($router->disabled == 'false') {

                try {
                    $this->testConnectionService->checkOnline($router);
                    $serverDisconnect = 'false';
                } catch (\Exception $e) {
                    $serverDisconnect = 'true';
                }
                if ($serverDisconnect == 'false') {
                    $customerSecretId = $customer->secret_id;
                    $userSecret = $this->secretService->getSecretById($router, $customerSecretId);

                    $profileName = Paket::where('id', $request->paket_id)->value('profile');
                    $username = $customer->username;
                    if ($userSecret != 'error') {

                        if ($autoIsolir != NULL && $autoIsolir->disabled == 'false') {
                            $secretProfiles = SecretResource::collection($userSecret)->value('profile');
                            if ($autoIsolir->profile_id != $secretProfiles) {
                                $response = $this->secretService->updateProfileSecret($router, $profileName, $customerSecretId);
                            } else {
                                $response = '';
                            }
                        } else {
                            $response = $this->secretService->updateProfileSecret($router, $profileName, $customerSecretId);
                        }

                        if (isset($response['after']['message'])) {
                            return redirect()->back()->with('error', $response['after']['message']);
                        } else if ($response == 'error') {
                            return redirect()->back()->with('error', 'Name secret ' . $username . ' not found on your server ' . $router->name . ' !');
                        } else {
                            $this->secretService->deleteActiveSecret($router, $username);
                            $customer->update([
                                'paket_id' => $request->paket_id,
                            ]);
                        }
                    } else {
                        return redirect()->back()->withErrors(['msg' => $customerSecretId . '! Canot find user "' . $customer->username . '" on server ' . $router->name . '.']);
                    }
                }
            }
        }

        if ($request->activation_date && $request->activation_date != $customer->activation_date) {
            if ($router->disabled == 'false' && $autoIsolir != NULL && $autoIsolir->disabled == 'false') {
                $periodeId = BillingPeriode::latest('created_at')->value('id');
                $statusPayment = Billing::where('customer_id', $customer->id)->where('billing_periode_id', $periodeId)->value('status');
                if ($statusPayment == 'LS') {
                    $month = Carbon::parse($request->activation_date)->addMonth()->format('d_m_Y');
                } else {
                    $month = Carbon::parse($request->activation_date)->format('d_m_Y');
                }
                $mikrotikId = $customer->mikrotik_id;
                $customerSecretId = $customer->secret_id;
                $router = Mikrotik::where('id', $mikrotikId)->first();
                $autoIsolir = AutoIsolir::where('mikrotik_id', $mikrotikId)->first();

                if ($autoIsolir->activation_date == 'true') {
                    try {
                        $comment_payment = $autoIsolir->comment_unpayment . '_' . $month;
                        dispatch(new AddPaymentBeforeDueDateJob($router, $comment_payment, $customerSecretId))->onQueue('default');
                    } catch (\Exception $e) {
                        return redirect()->back()
                            ->with('error', 'Server offline, please try again!.');
                    }
                }
                $customer->update(['activation_date' => $request->activation_date]);
            }
        }
        $customer->update([
            'name' => $request->name,
            'email' => $request->email,
            'address' => $request->address,
            'contact' => $request->contact,
            'discount' => $discount,
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
        ]);
        return redirect()->back()
            ->with('success', 'Customer ' . $customer->name . ' updated!');
    }


    public function editPpp(Customer $customer)
    {
        $router = Mikrotik::where('id', $customer->mikrotik_id)->first();
        try {
            $this->testConnectionService->checkOnline($router);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => 'Server offline, canot edit this data.']);
        }
        $services = PppType::orderBy('name', 'ASC')->pluck('name', 'id');
        return view('customers.editppp', compact('customer', 'services'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function updatePpp(CustomerPppUpdateRequest $request, Customer $customer)
    {
        $this->validate($request, [
            'username' => ['required', 'string', 'max:45', Rule::unique('customers')->ignore($customer->slug, 'slug')],
        ]);

        $customerSecretId = $customer->secret_id;
        $username = $request->username;
        $password = $request->password_ppp;
        $serviceName = PppType::where('id', $request->ppp_type_id)->value('name');
        $router = Mikrotik::where('id', $customer->mikrotik_id)->first();
        try {
            $response = $this->secretService->updateUserPasswordServiceSecret($router, $username, $password, $serviceName, $customerSecretId);
            if (isset($response['after']['message'])) {
                return redirect()->back()->with('error', $response['after']['message']);
            } else {
                if ($response == 'error') {
                    return redirect()->back()->with('error', 'Name secret ' . $username . ' not found on your server ' . $router->name . ' !');
                } else {
                    $customer->update([
                        'username' => $request->username,
                        'ppp_type_id' => $request->ppp_type_id,
                        'ppp_type_id' => $request->ppp_type_id
                    ]);
                    $this->secretService->deleteActiveSecret($router, $username);
                    return redirect()->back()
                        ->with('success', 'PPP updated successfully');
                }
            }
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => 'Router offline, please check your router.']);
        }
    }

    public function editRouter(Customer $customer)
    {
        $customerRouter = CustomerOnt::where('customer_id', $customer->id)->first();
        $merkRouters = MerkRouter::where('disabled', 'false')->orderBy('name', 'ASC')->get();
        $merkRouterId = MerkRouter::where('disabled', 'false')->where('name', $customerRouter->merk_ont)->value('id');
        $typeRouters = TypeRouter::where('disabled', 'false')->where('merk_router_id', $merkRouterId)->orderBy('name', 'ASC')->get();

        return view('customers.edit_router', compact('merkRouters', 'customer', 'customerRouter', 'typeRouters'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function updateRouter(CustomerRouterUpdateRequest $request, Customer $customer)
    {

        $customerId = $customer->id;

        $customer = CustomerOnt::where('customer_id', $customerId)->update([
            'merk_ont' => $request->merk_ont,
            'type_ont' => $request->type_ont,
            'username' => $request->username,
            'password' => $request->password,
            'port' => $request->port,
            'mac_address' => $request->mac_address,
            'description' => $request->description
        ]);

        return redirect()->back()
            ->with('success', 'Router updated successfully');
    }


    /**
     * Remove the specified resource from storage.
     */
    public function delete(Customer $customer)
    {
        return view('customers.delete', compact('customer'));
    }
    public function destroy(Customer $customer)
    {
        if ($customer->activation == 'true') {
            $router = Mikrotik::where('id', $customer->mikrotik_id)->first();
            try {
                $this->testConnectionService->checkOnline($router);
            } catch (\Exception $e) {
                return redirect()->back()->withErrors(['msg' => 'Router offline, please check your router.']);
            }
            $username = $customer->username;
            dispatch(new DeletePppSecretJob($router, $username))->onQueue('default');
        }

        //Backup Customer deleted
        DeletedCustomer::create([
            'name' => $customer->name,
            'slug' => $customer->slug,
            'password' => $customer->password,
            'email' => $customer->email,
            'paket_id' => $customer->paket->name,
            'mikrotik_id' => $customer->mikrotik->name,
            'secret_id' => $customer->secret_id,
            'username' => $customer->username,
            'password_ppp' => $customer->password_ppp,
            'ppp_type_id' => $customer->ppp_type->name,
            'address' => $customer->address,
            'contact' => $customer->contact
        ]);

        //Eksekusi delete
        $customer->delete();
        return redirect()->route('customers.index')
            ->with('success', 'Customer deleted successfully');
    }

    public function disable(Customer $customer)
    {
        $router = Mikrotik::where('id', $customer->mikrotik_id)->first();

        try {
            $this->testConnectionService->checkOnline($router);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => 'Router offline, please check your router.']);
        }

        $username = $customer->username;
        $job = new DisablePppSecretJob($router, $username);
        dispatch($job);
        $response = $job->getResponse();
        if ($response == 'success') {
            $customer->update([
                'disabled' => 'true'
            ]);
            return redirect()->back()->with('success', 'Disable customer successfully');
        } else {
            return redirect()->back()->withErrors(['msg' => 'Disable user failure, canot find user on mikrotik']);
        }
    }

    public function forceDisable(Customer $customer)
    {
        $customer->update([
            'disabled' => 'false'
        ]);
        return redirect()->back()->with('success', 'Disable customer successfully but not in server');
    }

    public function enable(Customer $customer)
    {
        $router = Mikrotik::where('id', $customer->mikrotik_id)->first();
        try {
            $this->testConnectionService->checkOnline($router);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => 'Router offline, please check your router.']);
        }
        $username = $customer->username;
        $job = new EnablePppSecretJob($router, $username);
        dispatch($job);
        $response = $job->getResponse();

        if ($response == 'success') {
            $customer->update([
                'disabled' => 'false'
            ]);
            return redirect()->back()->with('success', 'Enable customer successfully');
        } else {
            return redirect()->back()->withErrors(['msg' => 'Enable user failure, canot find user on mikrotik']);
        }
    }

    public function forceEnable(Customer $customer)
    {
        $customer->update([
            'disabled' => 'false'
        ]);
        return redirect()->back()->with('success', 'Enable customer successfully but not in server');
    }
    public function setting(Request $request): View
    {
        $mikrotiks = Mikrotik::where('disabled', 'false')->orderBy('name', 'ASC')->pluck('name', 'id');
        return view('customers.setting', compact('mikrotiks'));
    }

    public function exportToRouter(Request $request)
    {
        $this->validate($request, [
            'servers' => ['required'],
        ]);
        $mikrotikId = $request->servers;
        $customers = Customer::where('mikrotik_id', $mikrotikId)->pluck('username')->toArray();
        $collectCustomerUsernames = collect($customers);
        $router = Mikrotik::where('id', $mikrotikId)->first();
        try {
            $this->testConnectionService->checkOnline($router);
        } catch (\Exception $e) {
            return redirect()->back()->withErrors(['msg' => 'Router offline, please check your router.', 'Server ' . $router->name . ' Offline']);
        }
        $getAllSecret = $this->secretService->getAllPppSecrets($router);
        $collectionSecret = SecretResource::collection($getAllSecret);
        $secretNames = $collectionSecret->pluck(['name'])->toArray();
        $collectSecretNames = collect($secretNames);
        $customersNonSecret = $collectCustomerUsernames->diff($collectSecretNames);
        $countSecretExported = 0;
        foreach ($customersNonSecret as $customerNonSecret) {
            $customers = Customer::where('username', $customerNonSecret)->first();
            $service = PppType::where('id', $customers->ppp_type_id)->value('name');
            $profile = Paket::where('id', $customers->paket_id)->value('profile');
            $autoIsolir = AutoIsolir::where('mikrotik_id', $mikrotikId)->first();
            if ($autoIsolir != NULL) {
                $comment = $autoIsolir->comment_unpayment;
            } else {
                $comment = 'import_from_customer_management';
            }
            $response = $this->secretService->createSecret($router, $customers->username,  $customers->password_ppp, $service, $profile, $comment);
            if (isset($response['after']['message'])) {
                Customer::where('id', $customers->id)->update([
                    'secret_id' => 0,
                ]);
            } else {
                $countSecretExported++;
                Customer::where('id', $customers->id)->update([
                    'secret_id' => $response['after']['ret'],
                ]);
            }
        }
        return redirect()->back()->with('success', $countSecretExported . ' Customer successfully export to server ' . $router->name);
    }

    public function activation_all_customer(Request $request)
    {
        $mikrotikId = $request->servers_activation;
        $server = Mikrotik::where('id', $mikrotikId)->value('name');
        $activationDate = Carbon::now()->format('d F Y');
        $customers = Customer::where('mikrotik_id', $mikrotikId)->get();
        foreach ($customers as $customer) {
            $customer->update([
                'activation_date' => $activationDate,
                'activation' => 'true',
                'disabled' => 'false',
            ]);
        }
        return redirect()->back()
            ->with('success', 'Activated ' . count($customers) . ' customers on server ' . $server . '.');
    }


    public function showCustomerOffline()
    {
        $routersEnable = Mikrotik::where('disabled', 'false')->get();
        $data = array();
        if (count($routersEnable) == 0) {
            return redirect()->route('customers.index')->withErrors(['msg' => 'All server disable, please check your server setting.', 'Or you dont have server data.']);
        }
        $customersOffline = 0;
        foreach ($routersEnable as $routerEnable) {
            try {
                $this->testConnectionService->checkOnline($routerEnable);
            } catch (\Exception $e) {
                return redirect()->route('customers.index')->withErrors(['msg' => 'Server ' . $routerEnable->name . ' Offline.', 'Please enable first or check your configuration!']);
            }
            $usernameCustomers = Customer::where('mikrotik_id', $routerEnable->id)->pluck('username')->toArray();
            $collectCustomerUsernames = collect($usernameCustomers);
            $getAllSecretActive = $this->secretService->getPppActive($routerEnable);
            $collectionSecretActive = SecretResource::collection($getAllSecretActive);
            $secretActive = $collectionSecretActive->pluck(['name'])->toArray();
            $secretsNonActive = $collectCustomerUsernames->diff($secretActive);
            foreach ($secretsNonActive as $secretNonActive) {
                $customers = Customer::where('username', $secretNonActive)->first();
                $getAllSecretOffline = $this->secretService->getSecretByName($routerEnable, $secretNonActive);
                $collectionSecretOffline = SecretResource::collection($getAllSecretOffline);
                $collectionSecretOffline = $collectionSecretOffline->value('last-logged-out');
                $customers['last_logged_out'] = $collectionSecretOffline;
                $data[] = $customers;
                $customersOffline++;
            }
        }
        return view('customers.offline', compact('data', 'customersOffline'));
    }
}
