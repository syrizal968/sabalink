<?php

namespace App\Http\Controllers;

use App\Models\Paket;
use App\Models\Mikrotik;
use App\Models\MerkRouter;
use App\Models\TypeRouter;
use Illuminate\Http\Request;
use App\Services\Routerboard\ScriptService;
use App\Http\Resources\Pppoe\ProfileResource;
use App\Services\Routerboard\ScheduleService;
use App\Services\Routerboard\Ppp\SecretService;

class DropdownController extends Controller
{
    private SecretService $secretService;
    private ScriptService $scriptService;
    private ScheduleService $scheduleService;

    public function __construct(SecretService $secretService, ScriptService $scriptService, ScheduleService $scheduleService)
    {
        $this->secretService = $secretService;
        $this->scriptService = $scriptService;
        $this->scheduleService = $scheduleService;
    }

    /**
     * Write code on Method
     * 
     * @return response()
     */
    public function fetchPaket(Request $request)
    {
        $data['pakets'] = Paket::where('mikrotik_id', $request->mikrotik_id)
            ->where('disabled', 'false')
            ->orderBy('price', 'ASC')
            ->get(["name", "id", "price"]);
        return response()->json($data);
    }

    public function fetchProfile(Request $request)
    {
        $router = Mikrotik::where('id', $request->mikrotik_id)->first();
        $profiles = $this->secretService->getPppProfile($router);
        $pppProfiles = ProfileResource::collection($profiles);
        return response()->json($pppProfiles);
    }

    public function fetchScript(Request $request)
    {
        $router = Mikrotik::where('id', $request->mikrotik_id)->first();
        $scripts = $this->scriptService->getAllScript($router);
        $collectionScript = ProfileResource::collection($scripts);
        return response()->json($collectionScript);
    }

    public function fetchSchedule(Request $request)
    {
        $router = Mikrotik::where('id', $request->mikrotik_id)->first();
        $schedules = $this->scheduleService->getAllSchedule($router);
        $collectionSchedule = ProfileResource::collection($schedules);
        return response()->json($collectionSchedule);
    }

    public function fetchTypeRouter(Request $request)
    {
        $merkRouterId = MerkRouter::where('name', $request->merk_ont)->value('id');

        $data = TypeRouter::where('merk_router_id', $merkRouterId)
            ->where('disabled', 'false')
            ->orderBy('name', 'ASC')
            ->get(["name"]);

        return response()->json($data);
    }
}
