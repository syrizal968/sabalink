<?php

namespace App\Models;

use App\Models\Paket;
use App\Models\PppType;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

class Customer extends Authenticatable
{
    //use HasFactory;
    use HasApiTokens, HasFactory, Notifiable, HasRoles;
    //public $guarded = [];
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'slug',
        'email',
        'password',
        'paket_id',
        'mikrotik_id',
        'secret_id',
        'username',
        'password_ppp',
        'ppp_type_id',
        'address',
        'contact',
        'disabled',
        'activation',
        'activation_date',
        'customer_number',
        'latitude',
        'longitude',

    ];
    // public $guarded = [];
    public function getRouteKeyName(): string
    {
        return 'slug';
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
    ];
    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'password' => 'hashed',
    ];

    public function mikrotik(): BelongsTo
    {
        return $this->belongsTo(Mikrotik::class);
    }
    public function paket(): BelongsTo
    {
        return $this->belongsTo(Paket::class);
    }

    public function ppp_type(): BelongsTo
    {
        return $this->belongsTo(PppType::class);
    }

    // public function user(): BelongsTo
    // {
    //     return $this->belongsTo(User::class);
    // }
}
